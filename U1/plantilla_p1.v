(* Practica 1 *)

Section P1.
Variables A B C:Prop.

(* Ej 1.1 *)
Theorem e11: A->A.
Proof.
intros; try assumption.
Qed.

(* ALT
   intro.
   asumption *)

(* Ej 1.2 *)
Theorem e12: A->B->A.
Proof.
intros a b.
exact a.
Qed.

(* Ej 1.3 *)
Theorem e13: (A->(B->C))->(A->B)->(A->C).
Proof.
intros f g x.
  apply f.
  exact x.

  apply g.
  assumption.
Qed.

(*Ej 2.1 *)
Theorem e21: (A->B)->(B->C)->A->C.
Proof.
intros f g x.
exact (g (f x)).
Qed.

 (* Print e21. *)

(*Ej 2.2 *)
Theorem e22: (A->B->C)->B->A->C.
Proof.
intros a_b_c b a.
apply a_b_c.
assumption.
assumption.
Qed.

(*Ej 3.1 *)
Theorem e31_1: A->A->A.
Proof.
intros a a'.
exact a.
Qed.

Theorem e31_2: A->A->A.
Proof (fun (x:A) (y:A) => y).

(* Ej 3.2 *)
Theorem e32_1: (A->B->C)->A->(A->C)->B->C.
Proof.
intros f x g y.
apply g.
exact x.
Qed.

(* Ej 4.1 *)
Theorem e41: A -> ~~A.
Proof.
unfold not.
intros x f_not_x.
apply f_not_x.
exact x.
Qed.

(* Ej 4.2 *)
Theorem e42: A -> B -> (A /\ B).
Proof.
intros x y.
split ; try assumption.
Qed.

(* Ej 4.3 *)
Theorem e43: (A->B->C) -> (A/\B->C).
Proof.
intros f a_and_b.
elim a_and_b.
apply f.
Qed.

(* Ej 4.4 *)
Theorem e44: A->(A\/B).
Proof.
intro a.
left.
exact a.
Qed.

(* Ej 4.5 *)
Theorem e45: B->(A\/B).
Proof.
intro b.
right.
exact b.
Qed.

(* Ej 4.6 *)
Theorem e46: (A \/ B) -> (B \/ A).
Proof.
intros a_or_b.
elim a_or_b.

  intro a.
  right.
  exact a.

  intro b.
  left.
  exact b.
Qed.

Theorem e46_1: (A \/ B) -> (B \/ A).
Proof.
intros a_or_b.
elim a_or_b; intro; [right | left]; assumption.
Qed.

(* Ej 4.7 *)
Theorem e47: (A->C)->(B->C)->A\/B->C.
Proof.
intros f g a_or_b.
elim a_or_b; [apply f | apply g].
Qed.

(* Ej 4.8 *)
Theorem e48: False->A.
Proof.
intro bottom.
elim bottom.
Qed.

(* Ej 5.1 *)
Theorem e51: (A->B)-> ~B-> ~A.
Proof.
intros f not_b.
unfold not.
intro a.
apply not_b.
apply f.
assumption.
Qed.

(* Ej 5.2 *)
Theorem e52: ~(A/\~A).
Proof.
unfold not.
intro contradiccion.
elim contradiccion.
intros a not_a.
apply not_a.
exact a.
Qed.

(* Ej 5.3 *)
Theorem e53: (A->B)-> ~(A/\~B).
Proof.
intro f.
unfold not.
intro contradiccion.
elim contradiccion.
intros a not_b.
apply not_b.
apply f.
assumption.
Qed.

(* Ej 5.4 *)
Theorem e54: (A/\B)->~(A->~B).
Proof.
intro a_and_b.
elim a_and_b.
intros a b.
unfold not.
intro f ; apply f ; assumption.
Qed.

(* Ej 5.5 *)
Theorem e55: (~A /\ ~~A) -> False.
Proof.
intro contradiccion.
elim contradiccion.
unfold not.
intros not_a a.
exact (a not_a).
Qed.

(* Ej 6.1 *)
Theorem e61: (A\/B) -> ~(~A/\~B).
Proof.
intro a_or_b.
unfold not.
intro not_a_and_not_b.
elim not_a_and_not_b.
intros not_a not_b.
elim a_or_b; assumption.
Qed.

(* Ej 6.2 *)
Theorem e62: A\/B <-> B\/A.
Proof.
unfold iff.
split.

- intro a_or_b.
  elim a_or_b.
    * intro a; right; assumption.
    * intro a; left; assumption.

- intro b_or_a.
  elim b_or_a.
    * intro a; right; assumption.
    * intro a; left; assumption.
Qed.

Theorem e62_2: A\/B <-> B\/A.
Proof.
unfold iff.
split; intro h; elim h; intro a;
try (left;assumption); try (right;assumption).
Qed.

(* Ej 6.3 *)
Theorem e63: A\/B -> ((A->B)->B).
Proof.
intros a_or_b f.
elim a_or_b.
- intro a; apply f; assumption.
- intro; assumption. (* VER: Esto es el e31, como lo apico? *)
Qed.

End P1.


Section Logica_Clasica.
Variables A B C: Prop.

(* Ej 7.1 *)
Theorem e71: A \/ ~A -> ~~A->A.
Proof.
intros a_or_not_a a.
elim a_or_not_a.
- intro x; assumption. (* e31 *)
- intro not_a.
  elim a.
  exact not_a.
Qed.

Theorem e71_2: A \/ ~A -> ~~A->A.
Proof.
intros a_or_not_a a.
elim a_or_not_a.
- intro x; assumption.
- intros not_a.
  absurd (~ A).
    * assumption.
    * assumption.
Qed. 

(* Ej 7.2 *)
Theorem e72: A\/~A -> ((A->B) \/ (B->A)).
Proof.
intro a_or_not_a.
elim a_or_not_a.

  intro a.
  right.
  intro b.
  assumption.

  intro not_a.
  left.
  intro a.
  absurd A; assumption.
Qed.

(* Ej 7.3 *)
Theorem e73: (A \/ ~A) -> ~(A /\ B) -> ~A \/ ~B.
Proof.
intro a_or_not_a.
(* unfold not in a_or_not_a. *)
unfold not. (* VER: Aca no conviene hacer los intros juntos *)
intro not_a_and_b.
elim a_or_not_a.

  intro a.
  right.
  intro b.
  apply not_a_and_b.
  split; [exact a | exact b].

  intro not_a.
  left.
  intro a.
  apply not_a; assumption.
Qed.


Require Import Classical.
Check classic.

(* Ej 8.1 *)
Theorem e81: forall A:Prop, ~~A->A.
Proof.
intros a x.
elim (classic a).
- intro; assumption. (* e31 *)
- intro not_x.
  elim x.
  exact not_x.
Qed.

(* Ej 8.2 *)
Theorem e82: forall A B:Prop, (A->B)\/(B->A).
Proof.
intros x y.
elim (classic x).

  intro a.
  right.
  intro b.
  assumption.

  intro not_a.
  left.
  intro a.
  absurd x; assumption.
Qed.

(* Ej 8.3 *)
Theorem e83: forall A B:Prop, ~(A/\B)-> ~A \/ ~B.
Proof.
intros x y.
elim (classic x).

  intro a.
  unfold not.
  right.
  intro b.
  apply H.
  split; [exact a | exact b].

  intro not_a.
  unfold not.
  left.
  intro a.
  apply not_a; assumption.
Qed.

End Logica_Clasica.

Section Ej9.
(* Ej 9 *)
(* Definiciones *)
Variable NM RED CONS UTIL : Prop.

Hypothesis H1 : NM -> ~RED.
Hypothesis H2 : UTIL -> CONS.

Theorem ej9: NM /\ UTIL -> CONS /\ ~RED.
Proof.
intro nn_and_util.
elim nn_and_util.
intros nm util.
split.
- exact (H2 util).
- exact (H1 nm).
Qed.

(* Alternativa planteando las hipotesis
de forma mas naive *)
Hypothesis H3 : ~NM \/ ~RED.
Hypothesis H4 : CONS \/ ~UTIL.

Theorem ej9_alt: NM /\ UTIL -> CONS /\ ~RED.
Proof.
intro nm_and_util; elim nm_and_util.
intros nm util.
split.

(* Prueba de CONS *)
- elim H4.
    * intro; assumption.
    * intro not_util.
      absurd UTIL; [exact not_util | exact util].

(* Prueba de ~RED *)
- elim H3.
    * intro not_nm.
      absurd NM; [exact not_nm | exact nm].
    * intro;assumption.
Qed.

End Ej9.

Section Ej10.

Variables A B C:Prop.

Theorem e47_alt: (A->C)->(B->C)->A\/B->C.
Proof.
tauto.
Qed.

Theorem e54_alt: (A/\B)->~(A->~B).
Proof.
tauto.
Qed.

(*  Este no funciona y está bien que no funcione
Theorem e83_alt: forall A B:Prop, ~(A/\B)-> ~A \/ ~B.
Proof.
tauto.
Qed. *)

End Ej10.

Section Ej11.

Variable ESC ROJ KILT CAS DOM : Prop.

Hypothesis Hip1 : ~ESC -> ROJ.
Hypothesis Hip2 : KILT \/ ~ROJ.
Hypothesis Hip3 : CAS -> ~DOM.
Hypothesis Hip4 : DOM <-> ESC.
Hypothesis Hip5 : KILT -> ESC /\ CAS.
Hypothesis Hip6 : ESC -> KILT.

Theorem ej11: ~(ESC /\ ROJ /\ KILT /\ CAS /\ DOM).
Proof.
unfold not.
intro and.
elim and; intro esc; intro and1.
elim and1; intro roj; intro and2.
elim and2; intro kilt; intro and3.
elim and3; intro cas; intro dom.

absurd DOM.

  apply Hip3.
  exact cas.

  exact dom.
Qed.

Theorem ej11_alt: ~(ESC /\ ROJ /\ KILT /\ CAS /\ DOM).
Proof.
tauto.
Qed.

End Ej11.

Section Ej12.

Require Import Classical.
Check classic.

(* Ej 12 *)
(* Definiciones *)
Variable PF:Prop. (* el paciente tiene fiebre *)
Variable PA:Prop. (* el paciente tiene piel amarillenta *)
Variable PH:Prop. (* el paciente tiene hepatitis *)
Variable PR:Prop. (* el paciente tiene rubeola *)

Hypothesis Regla1: PF \/ PA -> PH \/ PR.
Hypothesis Regla2: ~PR \/ PF.
(* VER: Qué conviene? ~PR \/ PF ó PR -> PF? *)
Hypothesis Regla3: PH /\ ~PR -> PA.

Theorem ej12: (~PA /\ PF) -> PR.
Proof.
intro and; elim and; intros not_pa pf.
elim (classic PR).
-intro; assumption.
-intro not_pr.
 cut (PF \/ PA).
 intro pf_or_pa.
 apply Regla1 in pf_or_pa.
  elim pf_or_pa.
    intro ph.
    cut False.
    intro f.
    elim f.
    cut (PH /\ ~PR).
    intro ph_and_not_pr.
    apply Regla3 in ph_and_not_pr.
    absurd PA.
      exact not_pa.
      exact ph_and_not_pr.
    split.
    assumption.
    assumption.
  intro;assumption.
  left;assumption.
Qed.

(* VER: Que tauto funcione significa que existe una
prueba que no usa el tercero excluido? *)
Theorem ej12_alt: (~PA /\ PF) -> PR.
Proof.
tauto.
(* Show Proof. *)
Qed.

Print ej12_alt.

End Ej12.

Section EjerciciosExtra.

Require Import Classical.
Check classic.

Variables A B C: Prop.

Theorem ex1: A -> (B /\ C) -> (A -> B) /\ (A -> C).
Proof.
intros a b_and_c; elim b_and_c; intros b c.
split; intro; assumption. 
Qed.

(* VER: En realidad hay que probar ~~((A -> B) -> A) -> A. *)
(* Esto es una condicion suficiente para asumir que tengo
que usar el tercero excluído? *)
Theorem ex2: ((A -> B) -> A) -> A.
Proof.
tauto.
Qed.
Print ex2. (* Ver prueba tauto *)

Theorem ex2_2: ((A -> B) -> A) -> A.
Proof.
intro impl.
elim (classic A).
- intros a; assumption.
- intros not_a.
  apply impl.
  intro. contradiction.
Qed.

End EjerciciosExtra.
