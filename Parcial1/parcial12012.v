(* Problema 1, 2 en práctica *)

Section Problema3.
Require Import Classical.
Check classic.

Variable C : Set.
Variable P : C -> C -> Prop.

Lemma L3 : (exists x y : C, P x y) \/ ~(exists x : C, P x x).
(* Proof.
elim (classic (exists x y : C, P x y)); intros p_x_y.
- left; assumption.
- right.
  intro _p_x_x; elim _p_x_x; intros x p_x_x.
  apply p_x_y.
  exists x; exists x; assumption.
Qed. *)
Proof.
elim (classic (exists x : C, P x x)); intros H.
- left; elim H; intros; exists x; exists x; assumption.
- right; assumption.
Qed.

End Problema3.

Section Problema4.

Variable Nat: Set.
Variable zero: Nat.
Variable suc: Nat->Nat.
Variable sum: Nat->Nat->Nat.
Variable prod: Nat->Nat->Nat.
Axiom sum0 : forall n: Nat, sum n zero = n.
Axiom sumS : forall m n: Nat, sum m (suc n) = suc (sum m n).
Axiom prod0 : forall n: Nat, prod n zero = zero.
Axiom prodS : forall m n: Nat, prod m (suc n) = sum m (prod m n).
Axiom allNat : forall n: Nat, n = zero \/ exists m: Nat, suc m = n.
Axiom discNat : forall n: Nat, suc n <> zero.

Lemma L41: forall n: Nat, exists m: Nat, prod n (suc m) = sum n n.
Proof.
intro n.
exists (suc zero).
rewrite prodS.
rewrite prodS.
rewrite prod0.
rewrite sum0.
reflexivity.
Qed.

(* En la practica *)
(* Lemma L42: forall m n: Nat, m <> zero -> sum n m <> zero. *)

(* En la practica *)
(* Lemma L43: forall m n: Nat, sum m n = zero -> m = zero /\ n = zero. *)

End Problema4.

Section Ejercicio5.

Parameter type : Set.
Parameter TNat : type.
Parameter TBool : type.
Parameter exp : type -> Set.

Parameter NConst : nat -> exp TNat.
Parameter Plus : exp TNat -> exp TNat -> exp TNat.

Parameter BConst : bool -> exp TBool.
Parameter And : exp TBool -> exp TBool -> exp TBool.

Parameter Eq : exp TNat -> exp TNat -> exp TBool.

Parameter eval : exp TBool -> bool.

Parameter If_pedorro : forall (t: type), exp TBool -> exp t -> exp t -> exp t.
Parameter If : forall (t1 t2: type) (b : exp TBool), exp t1 -> exp t2 -> match (eval b) with
  | true => exp t1
  | false => exp t2
end.
End Ejercicio5.

Section Ejercicio6.

Variable D : Set.
Variable e : D.
Variable V : D -> D -> Prop.
Variable Q T : D -> Prop.

Lemma L6a: (forall x y: D, V x y -> V y x) -> exists z : D, V e z -> V z e.
Proof.
exact (fun h => ex_intro _ e (h e e)).
Qed.
Print L6a.

Lemma L6b: (forall x:D, Q x) \/ (forall y:D, T y) -> forall z:D, Q z \/ T z.
Proof.
intros; elim H; intros; [left | right]; exact (H0 z).
Qed.

(* Forma alternativa?
Proof.
exact (fun h => fun z => match h with
  | or_introl l => or_introl (l z)
  | or_intror r => or_intror (r z)
  end).
Qed.
*)

End Ejercicio6.