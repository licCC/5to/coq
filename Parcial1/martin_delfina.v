Section Problema1. (* PROBLEMA 1 *)

Variable NM RED CONS UTIL : Prop.

Hypothesis H1 : NM -> not RED.
Hypothesis H2 : CONS \/ not UTIL.

Lemma lema1 : ~CONS \/ RED -> ~NM \/ ~UTIL.
Proof.
intros or; elim H2; elim or; intros.
- absurd CONS.
    * assumption.
    * assumption.
- left.
  unfold not; intro nm.
  apply (H1 nm).
  assumption.
- right.
  exact H0.
- right.
  exact H0.
Qed.

(* Definido de forma un poco menos naive. *)
Lemma lema1_alt : NM /\ UTIL -> CONS /\ ~RED.
Proof.
intro nn_and_util.
elim nn_and_util.
intros nm util.
split.
- elim H2.
    * intro; assumption.
    * intro not_util. absurd UTIL; [exact not_util | exact util].
- exact (H1 nm).
Qed.

End Problema1.

Section Problema2. (* PROBLEMA 2 *)

Variable C : Set.
Variables T U : C -> C -> Prop.

Lemma lema2: (forall (x y : C), (U x y -> ~(T x y))) /\ (exists z : C, T z z) 
           -> exists w : C, ~(U w w).
Proof.
intros and; elim and; intros f_u_x_y_then_not_t_x_y e_t_z_z.
elim e_t_z_z; intros z t_z_z.
exists z.
unfold not; intro u_z_z.
apply (f_u_x_y_then_not_t_x_y z z).
- assumption.
- assumption.
Qed.

End Problema2.

Section Problema3. (* PROBLEMA 3 *)

Variable Bool: Set.
Variable TRUE : Bool.
Variable FALSE : Bool.

Variable Not : Bool -> Bool.
Variable Or : Bool -> Bool -> Bool.
Variable And : Bool -> Bool -> Bool.

Axiom BoolVal : forall b : Bool, b = TRUE \/ b = FALSE.

Axiom NotTrue : Not TRUE = FALSE.
Axiom NotFalse : Not FALSE = TRUE.

Axiom AndTrue : forall b : Bool, And TRUE b = b.
Axiom AndFalse : forall b : Bool, And FALSE b = FALSE.

Axiom OrTrue : forall b : Bool, Or TRUE b = TRUE.
Axiom OrFalse : forall b : Bool, Or FALSE b = b.

Lemma lema3_1 : forall b : Bool, Not (Not b) = b.
Proof.
intros b.
elim (BoolVal b); intros b_value; replace b.
- rewrite NotTrue.
  rewrite NotFalse.
  reflexivity.
- rewrite NotFalse.
  rewrite NotTrue.
  reflexivity.
Qed.

Lemma lema3_2 : forall b1 b2 : Bool, Not (Or b1 b2) = And (Not b1) (Not b2).
Proof.
intros b1 b2.
elim (BoolVal b2); elim (BoolVal b1); intros b1_v b2_v;
replace b1; replace b2.
- rewrite OrTrue.
  rewrite NotTrue.
  rewrite AndFalse.
  reflexivity.
- rewrite OrFalse.
  rewrite NotFalse.
  rewrite AndTrue.
  reflexivity.
- rewrite OrTrue.
  rewrite NotTrue.
  rewrite AndFalse.
  reflexivity.
- rewrite OrFalse.
  rewrite NotFalse.
  rewrite AndTrue.
  reflexivity.
Qed.

Lemma lema3_3 : forall b1 : Bool, exists b2 : Bool, And b1 b2 = Or b1 b2.
Proof.
intros b1.
elim (BoolVal b1); intros b1_v; rewrite b1_v.
- exists TRUE.
  rewrite AndTrue.
  rewrite OrTrue.
  reflexivity.
- exists FALSE.
  rewrite AndFalse.
  rewrite OrFalse.
  reflexivity.
Qed.

End Problema3.

Section Problema4. (* PROBLEMA 4 *)

Parameter Array : Set -> nat -> Set.
(* a*)
Parameter empty : forall (t : Set), Array t 0.
Check empty.

(* b *)
Parameter add : forall (t : Set) (n : nat), t -> t -> Array t n -> Array t (n + 2).
Check add.

(* c *)
Definition A2 := add nat 0 2 1 (empty nat). (* arreglo de largo 2 *)
Definition A4 := add nat 2 4 3 A2. (* arreglo de largo 4 *)
Definition A6 := add nat 4 6 5 A4. (* arreglo de largo 6 pedido con elementos del 1 al 6 *) 
Check A6.

(* d *)
Parameter add1 : forall (t : Set) (n : nat), t -> Array t n -> Array t (n + 1).

Definition A3 := add1 nat 2 9 (add nat 0 8 7 (empty nat)). (* arreglo de largo 3 pedido con elementos 7, 8 y 9 *)
Check A3.

End Problema4.

Section Problema5.

Variable S : Set.
Variable e1 e2 : S.
Variable P1 P2 : S -> S -> Prop.
Variable P3 P4 : S -> Prop.

Lemma lema5_1 : (forall x y : S, P1 x y -> P2 y x) -> (P1 e1 e2) ->  P2 e2 e1.
Proof.
     exact (fun (f : forall x y : S, P1 x y -> P2 y x) =>
              fun (p1_x_y : P1 e1 e2) => f e1 e2 p1_x_y).
Qed.

Lemma lema5_2 : (forall x:S, P3 x) \/ (forall y:S, P4 y) -> forall z:S, P3 z \/ P4 z.
Proof.
intros or x; elim or; intros p_x; [left | right]; exact (p_x x).
Qed. 

End Problema5.
