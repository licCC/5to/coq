# Entrega final de 'Construcción Formal de Programas en Teoría de Tipos'
Delfina Martín - UNR - 2023

La entrega consiste en 2 documentos, a saber, State.v y Actions.v.
En ambos documentos se incluyen definiciones alternativas para algunos ejercicios. Las versiones originales y las primadas son equivalentes. Se eligió en cada caso una versión por sobre la otra de manera que las pruebas a partir de aquellas definiciones fueran más sencillas, sintéticas y/o legibles.

**Importante** : en el archivo State.v linea 6 se debe reemplazar "" por la ruta absoluta al archivo State.