(*******************************************************************
 * Este archivo especifica las acciones Como transformadores de estado.
 * LAS DEFINICIONES DADAS PUEDEN SER USADAS DIRECTAMENTE O CAMBIADAS.
 ******************************************************************)

Load "".

Parameter ctxt : context.

Section Actions.

  Inductive Action :=
    | read: forall (va: vadd), Action
    | write: forall (va: vadd) (val: value), Action
    | chmod: Action.

  (* Action preconditions *)
  Definition Pre' (s : State) (a : Action) : Prop :=
    match a with
      | read va =>
          (ctxt_vadd_accessible ctxt) va = true /\
          aos_activity s = running /\
          exists (ma: madd), (va_mapped_to_ma s va ma /\
            match (memory s) ma with
              | Some page => is_RW (page_content page)
              | _ => False end)
      | write va _ =>
          (ctxt_vadd_accessible ctxt) va = true /\
          aos_activity(s) = running /\
          exists (ma: madd), (va_mapped_to_ma s va ma /\
            match (memory s) ma with
              | Some page => is_RW (page_content page)
              | _ => False end)
      | chmod =>
          (aos_activity s) = waiting /\
            match (oss s) (active_os s) with
              | Some os => hcall os = None
              | _ => False end
    end.

  (* Action preconditions *)
  Definition Pre (s : State) (a : Action) : Prop :=
    match a with
      | read va =>
          (ctxt_vadd_accessible ctxt) va = true /\
            aos_activity s = running /\
              exists (ma: madd) (p: page),
                va_mapped_to_ma s va ma /\
                  (memory s) ma = Some p /\
                    is_RW (page_content p)
      | write va _ =>
          (ctxt_vadd_accessible ctxt) va = true /\
            aos_activity(s) = running /\
              exists (ma: madd) (p: page),
                va_mapped_to_ma s va ma /\
                  (memory s) ma = Some p /\
                    is_RW (page_content p)
      | chmod =>
          (aos_activity s) = waiting /\
            exists (o: os),
              (oss s) (active_os s) = Some o /\
                hcall o = None
    end.

  Definition differ_at_most_memory (s s': State) (ma: madd) : Prop :=
    active_os s = active_os s'/\
      aos_exec_mode s = aos_exec_mode s'/\
        aos_activity s = aos_activity s' /\
          oss s = oss s' /\
            hypervisor s = hypervisor s' /\
              forall (m: madd),
                m <> ma -> memory(s) m = memory(s') m.

  Definition differ_chmod (s s': State)
    (oa: os_activity) (exec_mode: exec_mode) : Prop :=
    active_os s = active_os s' /\
      exec_mode = aos_exec_mode s'/\
        oa = aos_activity s'/\
          oss s = oss s' /\
            hypervisor s = hypervisor s' /\
              memory s = memory s'.

  (* Action postconditions *)
  Definition Post (s : State) (a : Action) (s' : State) : Prop :=
    match a with
      | read va => s = s'
      | write va val =>
          exists (ma: madd), (va_mapped_to_ma s va ma /\
            let page := mk_page (RW (Some val)) (Os (active_os s))
              in memory s' = update (memory s) ma page /\
                differ_at_most_memory s s' ma)
      | chmod =>
          (trusted_os ctxt s /\
            differ_chmod s s' running svc) \/
              (~ (trusted_os ctxt s) /\
                differ_chmod s s' running usr)
    end.


  (* if the hypervisor or a trusted OS is running the
     processor must be in supervisor mode *)
  Definition valid_state_iii (s : State) : Prop :=
    (* if the hypervisor is running or *)
    (aos_activity s = waiting \/
    (* trusted OS is running *)
     trusted_os ctxt s) ->
      (* the processor must be in supervisor mode *)
      svc = aos_exec_mode s.

  Definition inyective {A B : Set} (pmap : A ⇸ B) :=
    forall x y, pmap x = pmap y -> x = y.

  (* the hypervisor maps an OS physical address to a
     machine address owned by that same OS *)
  Definition valid_state_v (s : State) : Prop :=
    forall (ma: madd) (p: page) (pa: padd) (f: padd ⇸ madd),
      (hypervisor s) (active_os s) = Some f /\
        f pa = Some ma /\
          (memory s) ma = Some p /\
            inyective f /\
              page_owned_by p = Os (active_os s).

(* the hypervisor maps an OS physical address to a
     machine address owned by that same OS *)
  Definition valid_state_v' (s : State) : Prop :=
    forall (pa: padd),
      match (hypervisor s) (active_os s) with
        | Some f =>
        match f pa with
          | Some madd =>
          match (memory s) madd with
            | Some page =>
            match page_owned_by page with
              | Os id => inyective f /\
                id = active_os s
              | _ => False end
            | _ => False end
          | _ => False end
        | _ => False end.

  (* all page tables of an OS 'o' map accessible virtual
     addresses to pages owned by 'o' and not accessible
     ones to pages owned by the hypervisor. *)
  Definition valid_state_vi (s : State) : Prop :=
    forall (p mapped_p: page) (pt: vadd ⇸ madd) (va: vadd) (o: os_ident) (ma: madd),
      page_content p = PT pt ->
        page_owned_by p = Os o ->
          pt va = Some ma ->
            (memory s) ma = Some mapped_p ->
              ((ctxt_vadd_accessible ctxt) va = true ->
                  page_owned_by mapped_p = Os o) ->
                ((ctxt_vadd_accessible ctxt) va = false ->
                  page_owned_by mapped_p = Hyp).

  (* all page tables of an OS 'o' map accessible virtual
     addresses to pages owned by 'o' and not accessible
     ones to pages owned by the hypervisor. *)
  Definition valid_state_vi' (s : State) : Prop :=
    forall (p: page),
      match page_content p with
        | PT pt =>
        match page_owned_by p with
          | Os o => forall (va: vadd),
          match pt va with
            | Some ma =>
            match (memory s) ma with
              | Some mapped_p =>
                (ctxt_vadd_accessible ctxt) va = true ->
                  page_owned_by mapped_p = Os o /\
                (ctxt_vadd_accessible ctxt) va = false ->
                  page_owned_by mapped_p = Hyp
              | _ => False end
            | _ => False end
          | _ => True end
        | _ => True end.

  Definition valid_state (s : State) : Prop :=
    valid_state_iii s /\ valid_state_v s /\ valid_state_vi s.

  Inductive one_step : State -> Action -> State -> Prop :=
    comp: forall (s s': State) (a: Action),
      valid_state s -> Pre s a -> Post s a s' ->
        one_step s a s'.

  Notation "a ⇒ s ↪ s'" := (one_step s a s') (at level 50).

  Theorem one_step_preserves_prop_iii : forall (s s' : State) (a : Action),
      a ⇒ s ↪ s' -> valid_state_iii s'.
  Proof.
    intros.
    induction H; induction a.

    - rewrite <- H1.
      elim H; intros; assumption.

    - elim H; intros; clear H H3.

      elim H0; clear H0; intros H H0; clear H.
      elim H0; clear H0; intros Pre_aos_act H0; clear H0.

      elim H1; clear H1; intros ma' H1.
      elim H1; clear H1; intros H H1; clear H.
      elim H1; clear H1; intros H diff; clear H.

      inversion_clear diff.
      inversion_clear H0.
      inversion_clear H3; clear H4.

      unfold valid_state_iii; intro H'.
      rewrite <- H1.
      apply H2.

      elim H'; intros; [left | right].
      -- rewrite H0; assumption.
      -- unfold trusted_os; rewrite H; assumption.

    - elim H0; intros s_act H2; clear H2.
      elim H1; intros; elim H2; intros; clear H2;
      inversion H4; clear H4; inversion H5; clear H5;
      inversion H6; clear H6 H7;
      unfold valid_state_iii; intro H'.

      -- rewrite H4.
         reflexivity.
      -- elim H'; intros.
         --- rewrite H6 in H5.
             discriminate.
         --- unfold trusted_os in H3.
             rewrite H2 in H3.
             rewrite H6 in H3.
             contradiction.
  Qed.

  Theorem read_isolation : forall (s s' : State) (va : vadd),
    (read va) ⇒ s ↪ s' ->
      exists (ma: madd), va_mapped_to_ma s va ma /\
        exists (p: page), Some p = (memory s) ma /\
          page_owned_by p = Os (active_os s).
  Proof.
  intros.
  inversion H; clear H H3 H4 H5.

  inversion H1; clear H1.
  inversion H3; clear H1 H3.
  inversion H4; clear H4.
  inversion H1; clear H1.
  inversion H3; clear H3.
  inversion H4; clear H4 H5.

  inversion H0; clear H0.
  inversion H5; clear H4 H5 H6.

  inversion H2; clear H2.
  rewrite <- H4; clear H4.
  
  exists x.
  split.
  - assumption.
  - exists x0.
    split.
    -- symmetry; assumption.
    -- elim H1; intros os H2; clear H1.
       elim H2; intros hmap H4; clear H2.
       apply (H0 x x0 (curr_page os) hmap).
  Qed.

  Theorem read_isolation' : forall (s s' : State) (va : vadd),
    (read va) ⇒ s ↪ s' ->
      exists (ma: madd), va_mapped_to_ma s va ma /\
        exists (p: page), Some p = (memory s) ma /\
          page_owned_by p = Os (active_os s).
  Proof.
  intros.
  inversion_clear H.

  inversion_clear H1; clear H.
  inversion_clear H3; clear H.
  inversion_clear H1.
  inversion_clear H.
  inversion_clear H1.
  inversion_clear H3; clear H4.

  inversion_clear H0; clear H3.
  inversion_clear H4; clear H3.

  inversion H2; clear H2.
  rewrite <- H3; clear H3.
  
  exists x.
  split.
  - assumption.
  - exists x0.
    split.
    -- symmetry; assumption.
    -- elim H; intros os H2; clear H.
       elim H2; intros hmap H4; clear H2.
       apply (H0 x x0 (curr_page os) hmap).
  Qed.

End Actions.