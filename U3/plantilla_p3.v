Section Ejercicio1.
Variable A B C: Set.

Definition term1_1 : (A->A->A)->A->A := 
  fun f x => f x x.
Check term1_1.

Lemma term1_1_tauto: (A->A->A)->A->A.
Proof.
  tauto.
Qed.
Check term1_1_tauto.

Lemma term1_1_exact: (A->A->A)->A->A.
Proof.
  exact (fun f x => f x x).
Qed.

Lemma term1_2_exact: (A->B)->(B->C)->A->B->C.
Proof.
  exact (fun _ g _ y => g y).
Qed.

Lemma term1_3_exact: (A->B)->(A->B->C)->A->C.
Proof.
  exact (fun (f: A->B) (g: A->B->C) (x: A) => g x (f x)).
Qed.

Lemma term1_4_exact: (A->B)->((A->B)->C)->C.
Proof.
  exact (fun f g => g f).
Qed.

End Ejercicio1.

Section Ejercicio2.
Variable A B C: Set.

(*[x:A->B->C][y:A][z:B](x y z)*)
Definition term2_1: (A -> B -> C) -> A -> B -> C :=
  fun x y z => x y z.
Check term2_1.

(*[x:A->(B->C)][y:A][z:B]((x y) z)*)
Definition term2_2: (A -> B -> C) -> A -> B -> C :=
  fun x y z => (x y) z.
Check term2_2.

(*[z:B]([x:A->(B->C)][y:A]((x y) z))*)
(* El primero en otro orden pero ademas z puede aparecer en el tipo de x *)
Definition term2_aaaaaa: B -> (A -> B -> C) -> A -> C:=
  fun z x y => (x y) z.
Check term2_2.

(*[x:B->C][y:A->B][z:A](x (y z))*)
Definition term2_3: (B->C)->(A->B)->A->C :=
  fun x y z => x (y z).
Check term2_3.

(*[x:A->B](x [y:A]y)*)
Definition term2_4: ((A->A)->B)->B :=
  fun x => x (fun y => y).
Check term2_4.

End Ejercicio2.

Section Ejercicio3.
Variable A B C: Set.

Definition apply: (A->B)->A->B :=
  fun f x => f x.
Check apply.

Definition applyGen: forall A B: Set, (A->B)->A->B :=
  fun a b (f:a->b) (x:a) => f x.
Check applyGen.

Definition comp: (A->B)->(B->C)->(A->C) :=
  fun f g => fun x => g (f x).
Check comp.

Definition compGen: forall A B C: Set, (A->B)->(B->C)->(A->C) :=
  fun a b c (f:a->b) (g:b->c) => fun x => g (f x).
Check compGen.

Definition twice: (A->A)->A->A :=
  fun f x => f (f x).
Check twice.

Definition twiceGen: forall A: Set, (A->A)->A->A :=
  fun a (f:a->a) (x:a) => f (f x).
Check twiceGen.

End Ejercicio3.

Section Ejercicio4.
Variable A: Set.

Definition id := fun (x:A) => x.

Theorem e4 : forall x:A, (compGen A A A id id) x = id x.
Proof.
compute.
intros.
reflexivity.
Qed.

Theorem e4_1 : forall x:A, (compGen A A A id id) x = id x.
Proof.
intros.
cbv delta.
cbv beta.
reflexivity.
Qed.

Theorem e4_2 : forall x:A, (compGen A A A id id) x = id x.
Proof.
intros.
simpl.
reflexivity.
Qed.

End Ejercicio4.

Section Ejercicio5.

(* 5.1 *)
Definition opI (A : Set) (x : A) := x.

Definition opK (A B : Set) (x : A) (y : B) := x.

Definition opS (A B C : Set) (f : A -> B -> C) (g : A -> B) (x : A) := ((f x) (g x)).

(* 5.2 *)
(* Para formalizar el siguiente lema, determine los tipos ?1 ... ?8 adecuados *)
Lemma e52 : forall A B : Set, opS A (B -> A) A (opK A (B -> A)) (opK A B) = opI A.
Proof.
intros a b.
cbv delta.
cbv beta.
reflexivity.
Qed.

End Ejercicio5.


Section Ejercicio6.
Definition N := forall X : Set, X -> (X -> X) -> X.
Definition Zero (X : Set) (o : X) (f : X -> X) := o.
Definition Uno  (X : Set) (o : X) (f : X -> X) := f (Zero X o f).

(* 6.1 *)
Definition Dos (X : Set) (o : X) (f : X -> X) := f (Uno X o f).

(* 6.2 *)
Definition Succ (n : N) (X : Set) (o : X) (f : X -> X) := f (n X o f).

Lemma succUno : Succ Uno = Dos.
Proof.
cbv delta.
cbv beta.
reflexivity.
Qed.

(* 6.3 *)
Definition Plus (n m : N) : N
                := fun (X : Set) (o : X) (f : X -> X) => n X (m X o f) f.

Infix "_++" := Plus (left associativity, at level 94).
(* Print Nat.mul. *)

Lemma suma1: (Uno _++ Zero) = Uno.
Proof.
cbv delta.
cbv beta.
reflexivity.
Qed.

Lemma suma2: (Uno _++ Uno) = Dos.
Proof.
cbv delta.
cbv beta.
reflexivity.
Qed.

(* 6.4 *)
Definition Prod (n m : N) : N
                := fun (X:Set) (o:X) (f:X->X) => m X o (fun y:X => n X y f).


Infix "_**" := Prod (left associativity, at level 97).

(* 6.5 *)
Lemma prod1 : (Uno _** Zero) = Zero.
Proof.
cbv delta.
cbv beta.
reflexivity.
Qed.

Lemma prod2: (Uno _** Dos) = Dos.
Proof.
cbv delta.
cbv beta.
reflexivity.
Qed.

End Ejercicio6.

Section Ejercicio7.
(* 7.1 *)
Definition Bool := forall X : Set, X -> X -> X.
Definition t (A: Set) (x : A) (y : A) := x.
Definition f (A: Set) (x : A) (y : A) := y.

(* 7.2 *)
Definition If (c : Bool) (rt : Bool) (rf : Bool) (A : Set) (x y: A) :=
  c A (rt A x y) (rf A x y).
(* Ver que da tipos diferentes
Check If.
Check (If : Bool -> Bool -> Bool -> Bool).
*)

(* 7.3 *)
Definition Not (b : Bool) (A : Set) (t : A) (f : A) := b A f t.

Lemma CorrecNot : (Not t) = f /\ (Not f) = t.
Proof.
cbv delta.
cbv beta.
split.
- reflexivity.
- reflexivity.
Qed.

(* 7.4 *)
Definition And (b1 : Bool) (b2 : Bool) (A : Set) (x y : A) :=
  (If b1 b2 f) A x y.
(* b1 A (b2 A x y) (f A x y) *)
Check (And : Bool -> Bool -> Bool).

(* Supongo que en vez de b1 habrá que evaluar b2 *)
Definition And' (b1 : Bool) (b2 : Bool) (A : Set) (x y : A) :=
  (If b2 b1 f) A x y.

(* 7.5 *)
Infix "&" := And (left associativity, at level 81).

Lemma CorrecAnd : (t & t) = t /\ (f & t) = f /\ (t & f) = f.
Proof.
cbv delta.
cbv beta.
split.
- reflexivity.
- split.
  * reflexivity.
  * reflexivity.
Qed.

End Ejercicio7.

(* Ejercicio8 *)

Section ArrayNat.
Parameter ArrayNat : forall n:nat, Set.
Parameter empty    : ArrayNat 0.
Parameter add      : forall n:nat, nat -> ArrayNat n -> ArrayNat (n + 1).
(* Check (ArrayNat : nat -> Set). *)

(* 8.1 *)
Check ((add 0 (S 0) empty) : ArrayNat (0 + 1)).
Check ((add 0 (S 0) empty) : ArrayNat 1).

(* 8.2 *)
(* Vector de ceros de largo 2 *)
Check ((add (S 0) 0 (add 0 0 empty)) : ArrayNat 2).
Check ((add _ 0 (add _ 0 empty)) : ArrayNat 2).

(* Vector de largo 4 [0,1,0,1] *)
Check ((add _ 0 (add _ 1 (add _ 0 (add _ 1 empty)))) : ArrayNat 4).

(* 8.3 *)
Parameter Concat : forall n m:nat, ArrayNat n -> ArrayNat m -> ArrayNat (plus n m).

(* 8.4 *)
Parameter Zip : forall n m:nat, ArrayNat n -> ArrayNat n ->
  (nat -> nat -> nat) -> ArrayNat n.

(* 8.5 *)
Check (ArrayNat : nat -> Set).
(* En realidad no es tipo dependiente porque nat no ocurre en Set *)

(* 8.6 *)
Parameter ArrayGen : forall (t : Set) (n : nat), Set.
Parameter emptyGen : forall (t : Set), ArrayGen t 0.
Parameter addGen   : forall (t : Set) (n : nat), t -> ArrayGen t n -> ArrayGen t (n + 1).
Parameter ZipGen   : forall (t u v: Set) (n m:nat), ArrayGen t n -> ArrayGen u n ->
  (t -> u -> v) -> ArrayGen v n.

(* 8.7 *)
Parameter ArrayBool : forall (n : nat), ArrayGen bool n.
End ArrayNat.

Section Ejercicio9.
Parameter MatrizNat : forall (n:nat) (m:nat), Set.
Parameter emptyMz    : MatrizNat 0 0.
Parameter addMz      : forall (n:nat) (m:nat), nat -> nat -> MatrizNat n m -> MatrizNat (n + 1) m.

Parameter prod : forall (n m p:nat), MatrizNat n m -> MatrizNat m p -> MatrizNat n p.
Parameter es_id: forall (n m:nat), MatrizNat n m -> bool.
Parameter ins_fila: forall (n m:nat), MatrizNat n m -> ArrayNat m -> MatrizNat (n + 1) m.
Parameter ins_columna: forall (n m:nat), MatrizNat n m -> ArrayNat n -> MatrizNat n (m + 1).
(* (nat -> set) es el array *)
End Ejercicio9.

Section Ejercicio10.

Parameter Array : Set -> nat -> Set.
Parameter emptyA : forall X : Set, Array X 0.
Parameter addA : forall (X : Set) (n : nat), X -> Array X n -> Array X (S n).

Parameter Matrix : Set -> nat -> Set.
Parameter emptyM : forall X : Set, Matrix X 0.
(* Parameter emptyM : forall X : Set, Matrix emptyA 0. *)
Parameter addM : forall (X : Set) (n : nat), Array X (n + 1) -> Matrix X n -> Matrix X (n + 1).

(* Array de largo 1 con un único elemento 1 *)
Definition A1 := addA nat 0 1 (emptyA nat).
(* matriz de una columna *)
Definition M1 := addM nat 0 A1 (emptyM nat).

(* Array de largo 2 con elementos 2 *)
Definition A2 := addA nat 1 2 (addA nat 0 2 (emptyA nat)).
(* matriz de dos columnas *) 
Definition M2 := addM nat 1 A2 M1.

(* Array de largo 3 con elementos 3 *)
Definition A3 := addA nat 2 3 (addA nat 1 3 (addA nat 0 3 (emptyA nat))).
(* matriz de tres columnas *)
Definition M3 := addM nat 2 A3 M2.

Check M3.

End Ejercicio10.

Section Ejercicio11.
Require Import Nat.
(* Para poder usar power *)

Parameter ABNat : forall n : nat, Set.
Parameter emptyAB : ABNat 0.
Parameter addAB : forall (n:nat), ABNat n -> Array nat (2^n) -> ABNat (n + 1).

(* array: [2] *)
Definition AT1 := addA nat 0 2 (emptyA nat).
Definition T1 := addAB 0 emptyAB AT1.

(* array: [7 7] *)
Definition AT2 := addA nat 1 7 (addA nat 0 7 (emptyA nat)).
Definition T2 := addAB 1 T1 AT2.


(* array: [1 2 4 9] *)
Definition AT3 := addA nat 3 1 (addA nat 2 2 (addA nat 1 4 (addA nat 0 9 (emptyA nat)))).
Definition T3 := addAB 2 T2 AT3.

Parameter ABGen : forall (t: Set) (n: nat), Set.
Parameter emptyABGen : forall (t: Set), ABGen t 0.
Parameter addABGen : forall (t: Set) (n:nat), ABGen t n -> Array t (2^n) -> ABGen t (n + 1).
End Ejercicio11.

Section Ejercicio12.
Parameter AVLNat : forall n : nat, Set.
Parameter emptyAVLNat : AVLNat 0.
(* Mayor peso en el hijo izquierdo *)
Parameter addAVLNatL : forall (n : nat), nat -> AVLNat n -> AVLNat (n - 1) -> AVLNat (n + 1).
(* Mayor peso en el hijo derecho *)
Parameter addAVLNatR : forall (n : nat), nat -> AVLNat (n - 1) -> AVLNat n -> AVLNat (n + 1).
(* Hijos de igual peso *)
Parameter addAVLNatE : forall (n : nat), nat -> AVLNat n -> AVLNat n -> AVLNat (n + 1).

(* bfs del arbol a construir (donde E == empty): [8 7 6 4 E E 5] *)
Definition AVL_LL := addAVLNatE 0 4 emptyAVLNat emptyAVLNat.
Definition AVL_RR := addAVLNatE 0 5 emptyAVLNat emptyAVLNat.
Definition AVL_L := addAVLNatL 1 7 AVL_LL emptyAVLNat.
Definition AVL_R := addAVLNatR 1 6 emptyAVLNat AVL_RR.
Definition AVL_1 := addAVLNatE 2 8 AVL_L AVL_R.

Parameter AVLGen : forall (t : Set) (n : nat), Set.
Parameter emptyAVLGen : forall (t: Set), AVLGen t 0.
(* Mayor peso en el hijo izquierdo *)
Parameter addAVLGenL : forall (t: Set) (n : nat), nat -> AVLGen t n -> AVLGen t (n - 1) -> AVLGen t (n + 1).
(* Mayor peso en el hijo derecho *)
Parameter addAVLGenR : forall (t: Set) (n : nat), nat -> AVLGen t (n - 1) -> AVLGen t n -> AVLGen t (n + 1).
(* Hijos de igual peso *)
Parameter addAVLGenE : forall (t: Set) (n : nat), nat -> AVLGen t n -> AVLGen t n -> AVLGen t (n + 1).
End Ejercicio12.

Section Ejercicio13.
Variable A B C: Set.

Lemma e13_1 : (A -> B -> C) -> B -> A -> C.
Proof.
  exact (fun f x y => f y x).
Qed.

Lemma e13_2 : (A -> B) -> (B -> C) -> A -> C.
Proof.
  exact (fun f g x => g (f x)).
Qed.

Lemma e13_3 : (A -> B -> C) -> (B -> A) -> B -> C.
Proof.
  exact (fun f g x => f (g x) x).
Qed.

End Ejercicio13.

Section Ejercicio14.
Variable A B C: Prop.

Lemma Ej314_1 : (A -> B -> C) -> A -> (A -> C) -> B -> C.
Proof.
  intros _ a g _.
  exact (g a).
Qed.

Lemma Ej314_2 : A -> ~ ~ A.
Proof.
  unfold not.
  intros a not_a.
  exact (not_a a).
Qed.

Lemma Ej314_3 : (A -> B -> C) -> A -> B -> C.
Proof.
  exact (fun f x y => f x y).
Qed.

Lemma Ej314_4 : (A -> B) -> ~ (A /\ ~ B).
Proof.
  unfold not.
  intros f and.
  elim and; intros a not_b.
  exact (not_b (f a)).
Qed.

End Ejercicio14.

Section Ejercicio15.

Variable U : Set.
Variable e : U.
Variable A B : U -> Prop.
Variable P : Prop.
Variable R : U -> U -> Prop.

Lemma Ej315_1 : (forall x : U, A x -> B x) -> (forall x : U, A x) ->
forall x : U, B x.
Proof.
  intros implies exists_a a.
  exact (implies a (exists_a a)).
Qed.

Lemma Ej315_2 : forall x : U, A x -> ~ (forall x : U, ~ A x).
Proof.
  unfold not.
  intros a A_a not_a_x.
  exact (not_a_x a A_a).
Qed.

Lemma Ej315_3 : (forall x : U, P -> A x) -> P -> forall x : U, A x.
Proof.
    exact (fun p_a p => fun x => p_a x p).
Qed.

Lemma Ej315_4 : (forall x y : U, R x y) -> forall x : U, R x x.
Proof.
     exact (fun r_x_y => fun x => r_x_y x x).
Qed.

Lemma Ej315_5 : (forall x y: U, R x y -> R y x) ->
                 forall z : U, R e z -> R z e.
Proof.
  exact (fun r_x_y_r_y_x => fun z => r_x_y_r_y_x e z).
Qed.

End Ejercicio15.

