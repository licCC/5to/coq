Section WF.

Variable A : Type.

Variable le : A -> A -> bool.

Fixpoint insert (x : A) (ls : list A) : list A :=
  match ls with
    | nil => cons x nil
    | cons h ls' =>
      if le x h
        then cons x ls
        else cons h (insert x ls')
  end.

Fixpoint merge (ls1 ls2 : list A) : list A :=
  match ls1 with
    | nil => ls2
    | cons h ls' => insert h (merge ls' ls2)
  end.

Fixpoint split (ls : list A) : list A * list A :=
  match ls with
    | nil => (nil, nil)
    | cons h nil => (cons h nil, nil)
    | cons h1 (cons h2 ls') =>
        let (ls1, ls2) := split ls' in
          (cons h1 ls1, cons h2 ls2)
  end.

(* ill-formed *)
(* Fixpoint mergeSort (ls : list A): list A :=
  if Nat.leb (length ls) 1
    then ls
    else let lss := split ls in
      merge (mergeSort (fst lss)) (mergeSort (snd lss)). *)

Print well_founded.
Print Acc.

Require Import Coq.Lists.Streams.

CoInductive stream (A : Type) : Type :=
| Cons : A -> stream A -> stream A.

CoInductive infiniteDecreasingChain A (R : A -> A -> Prop) : stream A -> Prop :=
| ChainCons : forall x y s, infiniteDecreasingChain _ R (Cons _ y s)
  -> R y x
  -> infiniteDecreasingChain _ R (Cons _ x (Cons _ y s)).
  induction 1. (* induction en Acc R x *)
  intros s h.
  inversion h.
  apply (H0 y H4 s).
  rewrite <- H2.

  apply (H y H4).
    match goal with
      | [ H : infiniteDecreasingChain _ _ |- _ ] => inversion H; eauto
    end.
Qed.


End WF.