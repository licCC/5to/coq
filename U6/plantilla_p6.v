Section Ejercicio1.

Lemma predspec: forall (n : nat),
  {m : nat | n = 0 /\ m = 0 \/ n = S m}.
Proof.
intros.
destruct n.
- exists 0.
  left.
  split; reflexivity.
- exists n.
  right; reflexivity.
Qed.
Print predspec.
End Ejercicio1.

(* Require Import Coq.extraction.ExtrHaskellBasic.
Extraction Language Haskell.
Extraction "./predecesor" predspec. *)

Section Ejercicio2.

Inductive binTree (A: Set): Set :=
  | empty_bt: binTree A
  | add_bt: A -> binTree A -> binTree A -> binTree A.
(* Información en los nodos internos *)

Inductive mirror (A: Set): binTree A -> binTree A -> Prop :=
  | mirror_nil: mirror A (empty_bt A) (empty_bt A)
  | mirror_bt: forall (a:A) l1 r1 l2 r2,
      mirror _ l1 r2 -> mirror _ r1 l2 ->
        mirror _ (add_bt _ a l1 r1) (add_bt _ a l2 r2).

Fixpoint inverse (a: Set) (t: binTree a) {struct t}: binTree a :=
  match t with
    | empty_bt _ => empty_bt _
    | add_bt _ x l r =>
        add_bt _ x (inverse _ r) (inverse _ l)
  end.
(* Print inverse. *)

Lemma mirrorC: forall (A: Set) (t: binTree A),
  {t': binTree A | mirror A t t' }.
Proof.
intros.
induction t.
- exists (inverse A (empty_bt A)).
  simpl.
  constructor.
- elim IHt1; intros t1' proof_t1'.
  elim IHt2; intros t2' proof_t2'.
  exists (add_bt A a t2' t1').
  constructor; [exact proof_t1' | exact proof_t2'].
Qed.

Require Import FunInd.
Functional Scheme inverse_ind := Induction for inverse Sort Set.
Hint Constructors mirror: core.

Lemma mirrorC2: forall (A: Set) (t: binTree A),
  {t': binTree A | mirror A t t'}.
Proof.
intros.
exists (inverse A t).
functional induction (inverse A t); auto.
Qed.

End Ejercicio2.

(* Require Import Coq.extraction.ExtrHaskellBasic.
Extraction Language Haskell.
Extraction "./mirror_function.hs" mirrorC.
Extraction "./mirror_function2.hs" mirrorC2. *)

Section Ejercicio3.

Definition Value := bool.

Inductive BoolExpr : Set :=
  | bbool : bool -> BoolExpr
  | band : BoolExpr -> BoolExpr -> BoolExpr
  | bnot : BoolExpr -> BoolExpr.

Inductive BEval : BoolExpr -> Value -> Prop :=
  | ebool : forall b : bool, BEval (bbool b) (b:Value)
  | eandl : forall e1 e2 : BoolExpr, BEval e1 false -> BEval (band e1 e2) false
  | eandr : forall e1 e2 : BoolExpr, BEval e2 false -> BEval (band e1 e2) false
  | eandrl : forall e1 e2 : BoolExpr,
      BEval e1 true -> BEval e2 true -> BEval (band e1 e2) true
  | enott : forall e : BoolExpr, BEval e true -> BEval (bnot e) false
  | enotf : forall e : BoolExpr, BEval e false -> BEval (bnot e) true.

Fixpoint beval1 (e : BoolExpr) : Value :=
  match e with
    | bbool b => b
    | band e1 e2 =>
    match beval1 e1, beval1 e2 with
      | true, true => true
      | _, _ => false
    end
    | bnot e1 => if beval1 e1 then false else true
  end.

Fixpoint beval2 (e : BoolExpr) : Value :=
  match e with
    | bbool b => b
    | band e1 e2 => match beval2 e1 with
                      | false => false
                      | _ => beval2 e2
                    end
    | bnot e1 => if beval2 e1 then false else true
end.

Lemma beval1_verification: forall e:BoolExpr, {b:Value |(BEval e b)}.
Proof.
intros.
exists (beval1 e).
induction e; auto; simpl.
- constructor.
- case_eq (beval1 e1); case_eq (beval1 e2); intros.
  * apply (eandrl e1 e2); rewrite <- H0.
    ** exact IHe1.
    ** rewrite <- H0 in H.
       rewrite H in IHe2.
       exact IHe2.
  * apply (eandr e1 e2).
    rewrite H in IHe2.
    exact IHe2.
  * constructor.
    rewrite H0 in IHe1.
    exact IHe1.
  * constructor.
    rewrite H0 in IHe1.
    exact IHe1.
- case_eq (beval1 e); intros;
  rewrite H in IHe; constructor;
  assumption.
Qed.

Require Import FunInd.
Functional Scheme beval1_ind := Induction for beval1 Sort Set.
Hint Constructors BEval: core.

Lemma beval1_verification2: forall e:BoolExpr, {b:Value |(BEval e b)}.
Proof.
intros.
exists (beval1 e).
functional induction (beval1 e); auto.
- constructor; [rewrite e3 in IHv | rewrite e4 in IHv0]; assumption.
- rewrite e4 in IHv0; exact (eandr e1 e2 IHv0).
- constructor; rewrite e3 in IHv; assumption.
- constructor; rewrite e2 in IHv; assumption.
- constructor; rewrite e2 in IHv; assumption.
Qed.

Lemma beval2_verification: forall e:BoolExpr, {b:Value |(BEval e b)}.
Proof.
intros.
exists (beval2 e).
induction e; auto; simpl.
- case_eq (beval2 e1); intros; rewrite H in IHe1.
  -- destruct (beval2 e2).
     --- apply (eandrl e1 e2 IHe1 IHe2).
     --- apply (eandr e1 e2 IHe2).
  -- constructor; exact IHe1.
- case_eq (beval2 e); intros; rewrite H in IHe;
  constructor; assumption.
Qed.

Require Import FunInd.
Functional Scheme beval2_ind := Induction for beval2 Sort Set.

Lemma beval2_verification2: forall e:BoolExpr, {b:Value |(BEval e b)}.
Proof.
intros.
exists (beval2 e).
functional induction (beval2 e); auto.
- rewrite e3 in IHv; destruct (beval2 e2).
  * constructor; assumption.
  * apply (eandr e1 e2 IHv0).
- constructor; rewrite e3 in IHv; assumption.
- constructor; rewrite e2 in IHv; assumption.
- constructor; rewrite e2 in IHv; assumption.
Qed.

End Ejercicio3.

(* Require Import Coq.extraction.ExtrHaskellBasic.
Extraction Language Haskell.
Extraction "./beval1.hs" beval1_verification.
Extraction "./beval2.hs" beval2_verification. *)

Section Ejercicio4.

Variable A: Set.

Inductive list : Set :=
  | nil: list
  | cons: A -> list -> list.

Fixpoint append (l1 l2: list) {struct l1} : list :=
  match l1 with
    | nil => l2
    | cons a l => cons a (append l l2)
  end.

Inductive perm : list -> list -> Prop :=
  | perm_refl: forall l, perm l l
  | perm_cons: forall a l0 l1, perm l0 l1 ->
      perm (cons a l0) (cons a l1)
  | perm_app: forall a l, perm (cons a l) (append l (cons a nil))
  | perm_trans: forall l1 l2 l3,
      perm l1 l2 -> perm l2 l3 -> perm l1 l3.

Hint Constructors perm: core.

Fixpoint reverse (l: list) {struct l} : list :=
  match l with
    | nil => nil
    | cons a l' => append (reverse l') (cons a nil)
  end.

Lemma Ej6_4: forall l: list, {l2: list | perm l l2}.
Proof.
intros.
exists (reverse l).
induction l.
- constructor.
- apply (perm_trans (cons a l)
                    (cons a (reverse l))
                    (append (reverse l) (cons a nil))).
  * exact (perm_cons a l (reverse l) IHl).
  * exact (perm_app a (reverse l)).
Qed.

End Ejercicio4.

Section Ejercicio5.

Inductive leq: nat -> nat -> Prop :=
  | leq0 : forall n:nat, leq 0 n
  | leqS : forall n m:nat, leq n m -> leq (S n) (S m).

Inductive geq: nat -> nat -> Prop :=
  | geq0 : forall n:nat, geq n 0
  | geqS : forall n m:nat, geq n m -> geq (S n) (S m).

Lemma le_gt_dec: forall n m:nat, {(leq n m)} + {(geq n m)}.
Proof.
induction n; induction m.
- left; exact (leq0 0).
- left; exact (leq0 (S m)).
- right; exact (geq0 (S n)).
- elim (IHn m); intros; [left | right];
  [apply (leqS)| apply (geqS)]; assumption.
Qed.

(* nativa: beq_nat *)
Fixpoint le_bool (n m: nat) {struct n}: bool :=
  match n with
    | 0 => true
    | S n' => match m with
        | 0 => false
        | S m' => le_bool n' m'
        end
  end.

Require Import FunInd.
Functional Scheme le_bool_rec := Induction for le_bool Sort Set.
Hint Constructors leq: core.
Hint Constructors geq: core.

Lemma le_gt_dec_2: forall n m:nat, {(leq n m)} + {(geq n m)}.
Proof.
intros.
functional induction (le_bool n m); auto.
- elim IHb; intros;
  [left | right]; [apply leqS | apply geqS]; assumption.
Qed.

Require Import Lia.

Lemma native_le_gt_dec: forall n m:nat, {(le n m)} + {(gt n m)}.
Proof.
intros.
functional induction (le_bool n m); auto.
- left; lia.
- right; lia.
- elim IHb; intros; [left | right]; lia.
Qed.

End Ejercicio5.

Section Ejercicio6.

Definition spec_res_nat_div_mod (a b:nat) (qr:nat*nat) :=
  match qr with
    (q,r) => (a = b*q + r) /\ r < b
  end.

Require Import Lia.
Require Import DecBool.
Require Import Compare_dec.
Require Import Plus.
Require Import Mult.

Definition nat_div_mod: forall a b: nat,
  not (b = 0) -> {qr: nat * nat | spec_res_nat_div_mod a b qr}.
Proof.
induction a; intros.
- exists (0,0).
  unfold spec_res_nat_div_mod.
  lia.
- elim (IHa b H).
  intros qr' e.
  destruct qr' as (q, r).
  case_eq (lt_dec r (b-1)); intros h h'.
  * exists (q, r+1).
    unfold spec_res_nat_div_mod in *. (* idem simpl *)
    split; lia.
  * exists (q+1, 0).
    unfold spec_res_nat_div_mod in *.
    split; lia.
Qed.

End Ejercicio6.

Section Ejercicio7.

Inductive tree (A:Set): Set :=
  | leaf : tree A
  | node : A -> tree A -> tree A -> tree A.

Inductive tree_sub (A:Set) (t:tree A) : tree A -> Prop :=
  | tree_sub1: forall (t':tree A) (x:A), tree_sub A t (node A x t t')
  | tree_sub2: forall (t':tree A) (x:A), tree_sub A t (node A x t' t).

Theorem well_founded_tree_sub: forall A: Set, well_founded (tree_sub A).
Proof.
intros.
unfold well_founded; intros t.
induction t.
- apply Acc_intro. (* idem constructor *)
  intros t' leaf_y.
  inversion leaf_y.
- constructor.
  intros t' HI.
  inversion HI; [exact IHt1 | exact IHt2].
Qed.

End Ejercicio7.

Section Ejercicio8.

Fixpoint size (e: BoolExpr) {struct e}: nat :=
  match e with
    | bbool b => 1
    | band e1 e2 => size e1 + size e2
    | bnot e' => (size e') + 1
  end.

Definition elt (e1 e2 : BoolExpr) := size e1 < size e2.

Require Import Lia.

Lemma size_neq_0: forall (e: BoolExpr), size e <> 0.
Proof.
induction e; simpl; lia.
Qed.

Require Import Coq.Wellfounded.Inverse_Image.
Require Import Wf_nat.

Print wf_inverse_rel.
Print lt_wf.
Print wf_inverse_image.

Theorem well_founded_elt: well_founded elt.
Proof.
exact (wf_inverse_image BoolExpr nat lt size lt_wf).
Qed.

End Ejercicio8.

Section Ejercicio9.

Inductive sorted (A: Set) (R: A -> A -> Prop): list A -> Prop :=
  | sorted_nil: sorted A R (nil A)
  | sorted_one: forall (a:A), sorted A R (cons A a (nil A))
  | sorted_cons: forall (a b: A) l,
      R a b -> sorted A R (cons A b l) ->
        sorted A R (cons A a (cons A b l)).

Inductive p (A: Set): list A -> list A -> Prop :=
  | p_refl: forall l, p A l l
  | p_cons: forall a l0 l1, p A l0 l1 ->
      p A (cons _ a l0) (cons _ a l1)
  | p_ccons: forall a b l, p A (cons _ a (cons _ b l)) (cons _ b (cons _ a l))
  | p_trans: forall l1 l2 l3,
      p A l1 l2 -> p A l2 l3 -> p A l1 l3.

Fixpoint insert_sort (l: list nat) (n: nat) {struct l}: list nat :=
  match l with
    | nil _ => cons _ n (nil _)
    | cons _ x l' => if le_bool x n
        then cons _ x (insert_sort l' n)
        else cons _ n (insert_sort l' x)
  end.

Fixpoint insertion_sort (l: list nat) {struct l}: list nat :=
  match l with
    | nil _ => nil _
    | cons _ x l' => insert_sort (insertion_sort l') x
  end.

Lemma insertion_sorted: forall (l: list nat) (x: nat),
  sorted nat le (insertion_sort l).
Proof.
Admitted.

Lemma Sort: forall l: (list nat),
  {s: list nat | (sorted nat le s) /\ (p nat l s)}.
Proof.
intros.
induction l.
- exists (nil nat).
  split; constructor.
- destruct IHl.
  elim a0; intros.
  exists (insertion_sort a x).
  split.
  -- apply insertion_sorted.
  -- induction l; induction x.
      --- simpl. apply p_refl.
      --- simpl.
          admit.
      --- admit.
      --- admit.
Admitted.

End Ejercicio9.
