module Mirror_function2 where

import qualified Prelude

type Sig a = a
  -- singleton inductive, whose constructor was exist
  
data BinTree a =
   Empty_bt
 | Add_bt a (BinTree a) (BinTree a)

inverse :: (BinTree a1) -> BinTree a1
inverse t =
  case t of {
   Empty_bt -> Empty_bt;
   Add_bt x l r -> Add_bt x (inverse r) (inverse l)}

mirrorC2 :: (BinTree a1) -> (BinTree a1)
mirrorC2 =
  inverse

