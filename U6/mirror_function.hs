module Mirror_function where

import qualified Prelude

__ :: any
__ = Prelude.error "Logical or arity value used"

type Sig a = a
  -- singleton inductive, whose constructor was exist
  
sig_rect :: (a1 -> () -> a2) -> a1 -> a2
sig_rect f s =
  f s __

sig_rec :: (a1 -> () -> a2) -> a1 -> a2
sig_rec =
  sig_rect

data BinTree a =
   Empty_bt
 | Add_bt a (BinTree a) (BinTree a)

binTree_rect :: a2 -> (a1 -> (BinTree a1) -> a2 -> (BinTree a1) -> a2 -> a2)
                -> (BinTree a1) -> a2
binTree_rect f f0 b =
  case b of {
   Empty_bt -> f;
   Add_bt y b0 b1 -> f0 y b0 (binTree_rect f f0 b0) b1 (binTree_rect f f0 b1)}

binTree_rec :: a2 -> (a1 -> (BinTree a1) -> a2 -> (BinTree a1) -> a2 -> a2)
               -> (BinTree a1) -> a2
binTree_rec =
  binTree_rect

inverse :: (BinTree a1) -> BinTree a1
inverse t =
  case t of {
   Empty_bt -> Empty_bt;
   Add_bt x l r -> Add_bt x (inverse r) (inverse l)}

mirrorC :: (BinTree a1) -> (BinTree a1)
mirrorC t =
  binTree_rec (inverse Empty_bt) (\a _ iHt1 _ iHt2 ->
    sig_rec (\t1' _ -> sig_rec (\t2' _ -> Add_bt a t2' t1') iHt2) iHt1) t

