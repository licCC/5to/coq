Section Ejercicio1.

Variable U  : Set.
Variable A B: U -> Prop.
Variable P Q: Prop.
Variable R S: U -> U -> Prop.

Theorem e11 : (forall x:U, A(x)) -> forall y:U, A(y).
Proof.
intro a_x; apply a_x.
Qed.

Theorem e12 : (forall x y:U, (R x y)) -> forall x y:U, (R y x).
Proof.
intro r_x_y.
intro x; intro y.
apply r_x_y.
(* mas explicitamente
   apply (r_x_y y x). *)
Qed.


Theorem e13 : (forall x: U, ((A x)->(B x)))
                        -> (forall y:U, (A y))
                          -> (forall z:U, (B z)).
Proof.
intros a_x_to_b_x a_y z.
apply (a_x_to_b_x z).
apply (a_y z).
Qed.

End Ejercicio1.

Section Ejercicio2.

Variable U  : Set.
Variable A B: U -> Prop.
Variable P Q: Prop.
Variable R S: U -> U -> Prop.

Theorem e21 : (forall x:U, ((A x)-> ~(forall x:U, ~ (A x)))).
Proof.
intros x a_x.
unfold not.
intro not_a_y.
apply (not_a_y x).
exact a_x.
Qed.

Theorem e22 : (forall x y:U, ((R x y)))-> (forall x:U, (R x x)).
Proof.
intros r_x_y x.
apply (r_x_y x).
Qed.

Theorem e23 : (forall x:U, ((P -> (A x))))
                        -> (P -> (forall x: U, (A x))).
Proof.
intros a_x p x.
apply a_x.
exact p.
Qed.


Theorem e24 : (forall x:U, ((A x) /\ (B x)))
                        -> (forall x:U, (A x))
                          -> (forall x:U, (B x)).
Proof.
intros a_x_and_b_x a_x x.
apply (a_x_and_b_x x).
Qed.


Theorem e25 : (forall x:U, (A x)) \/ (forall x:U, (B x)) -> 
                      forall x:U, ~(~(A x) /\ ~(B x)).
Proof.
intros a_x_or_b_x x.
unfold not; intro not_a_x_and_not_b_x.
elim not_a_x_and_not_b_x; intros not_a_x not_b_x.
elim a_x_or_b_x.
- intro a_x.
  apply not_a_x.
  exact (a_x x).
- intro b_x.
  apply not_b_x.
  exact (b_x x).
Qed.

Theorem e25_2 : (forall x:U, (A x)) \/ (forall x:U, (B x)) -> 
                      forall x:U, (A x) \/ (B x).
Proof.
intros a_x_or_b_x x.
elim a_x_or_b_x; [left | right ]; apply H.
Qed.

End Ejercicio2.

Section Ejercicio3.

Variable U   : Set.
Variable A B : U -> Prop.
Variable P Q : Prop.
Variable R S : U -> U -> Prop.

Definition Reflexiva := forall x:U, (R x x).
Definition Simetrica := forall x y:U, R x y -> R y x.
Definition Transitiva := forall x y z:U, R x y /\ R y z -> R x z.

Definition H1 := Reflexiva.
Definition H2 := forall x y z:U, (R x y) /\ (R x z) -> (R y z).

Theorem e231: H1 /\ H2 -> Reflexiva /\ Simetrica /\ Transitiva.
Proof.
intro h1_and_h2; elim h1_and_h2; intros h1 h2.
split.

(* Reflexividad *)
exact h1.
split.

(* Simetria *)
intros x y r_x_y.
apply (h2 x y x).
split; [exact r_x_y | exact (h1 x)].

(* Transitividad *)
intros x y z r_x_y_and_r_y_z.
apply (h2 y x z).
elim r_x_y_and_r_y_z; intros r_x_y r_y_z.
split.
-apply (h2 x y x);split; [exact r_x_y | exact (h1 x)].
-exact r_y_z.
Qed.

Definition Asimetrica := forall x y:U, R x y -> ~ R y x.
Definition Irreflexiva := forall x:U, ~ (R x x).

Lemma e232 : Asimetrica -> Irreflexiva.
Proof.
intros asim x.
unfold not; intro r_x_x.
apply (asim x x); exact r_x_x.
Qed.

End Ejercicio3.

Section Ejercicio4.

Variable U : Set.
Variable A B : U->Prop.
Variable R : U->U->Prop.

Theorem e41: (exists x:U, exists y:U, (R x y)) -> 
                exists y:U, exists x:U, (R x y).
Proof.
intro r_x_y; elim r_x_y.
intros x r_x0_y.
elim r_x0_y.
intros y r_x0_y0.
exists y; exists x.
exact r_x0_y0.
Qed.

Theorem e42: (forall x:U, A(x)) -> 
                ~ exists x:U, ~ A(x).
Proof.
intro forall_a_x.
unfold not.
intro exists_not_a_x; elim exists_not_a_x.
intros x0 not_ax.
absurd (A x0).
- exact not_ax.
- apply forall_a_x.
Qed.

Theorem e43: (exists x:U, ~(A x)) -> 
                ~(forall x:U, (A x)).
Proof.
intro exists_not_a_x; elim exists_not_a_x.
unfold not; intros x not_a_x forall_a_x.
absurd (A x).
- exact not_a_x.
- apply forall_a_x.
Qed.

Theorem e44: (forall x:U, ((A x) /\ (B x)))
                -> (forall x:U, (A x)) /\ (forall x:U, (B x)).
Proof.
intro forall_ax_and_bx.
split.
- intro x.
  apply forall_ax_and_bx.
- intro x.
  apply forall_ax_and_bx.
Qed.
(* intro x; apply forall_ax_and_bx. *)

Theorem e45: (exists x:U, (A x \/ B x))->
                (exists x:U, A x) \/ (exists x:U, B x).
Proof.
intro exists_a_x_or_b_x; elim exists_a_x_or_b_x.
intros x a_x_or_b_x.
elim a_x_or_b_x.
- intro a_x.
  left.
  exists x.
  exact a_x.
- intro b_x.
  right.
  exists x.
  exact b_x.
(* intro h; [left | right]; exists x; exact h. *)
Qed.

Theorem e46: (forall x:U, A x) \/ (forall y:U, B y) 
                -> forall z:U, A z \/ B z.
Proof.
intro h; elim h.
- intro forall_a_x.
  intro x.
  left.
  apply forall_a_x.
- intro forall_b_y.
  intro y.
  right.
  apply forall_b_y.
Qed.

End Ejercicio4.

Section Ejercicio5.

Variable nat      : Set.
Variable S        : nat -> nat.
Variable a b c    : nat.
Variable odd even : nat -> Prop.
Variable P Q      : nat -> Prop.
Variable f        : nat -> nat.

Theorem e51: forall x:nat, exists y:nat, (P(x)->P(y)).
Proof.
intro x.
exists x.
intro; assumption.
Qed.

Theorem e52: exists x:nat, (P x)
                            -> (forall y:nat, (P y)->(Q y))
                               -> (exists z:nat, (Q z)).
Proof.
exists a.
intros p_a p_y_q_y.
exists a.
apply p_y_q_y.
exact p_a.
Qed.

Theorem e53: even(a) -> (forall x:nat, (even(x)->odd (S(x)))) -> exists y: nat, odd(y).
Proof.
intros even_a even_x_odd_s_x.
exists (S a).
apply (even_x_odd_s_x a).
exact even_a.
Qed.

Theorem e54: (forall x:nat, P(x) /\ odd(x) -> even(f(x)))
                            -> (forall x:nat, even(x) -> odd(S(x)))
                            -> even(a)
                            -> P(S(a))
                            -> exists z:nat, even(f(z)).
Proof.
intros p_x_odd_x_even_f_x even_x_odd_s_x even_a p_s_a.
exists (S a).
apply (p_x_odd_x_even_f_x (S a)).
split.
- exact p_s_a.
- apply (even_x_odd_s_x a); exact even_a.
Qed.

End Ejercicio5.

Section Ejercicio6.

Variable nat : Set.
Variable S   : nat -> nat.
Variable le  : nat -> nat -> Prop.
Variable f   : nat -> nat.
Variable P   : nat -> Prop.

Axiom le_n: forall n:nat, (le n n).
Axiom le_S: forall n m:nat, (le n m) -> (le n (S m)).
Axiom monoticity: forall n m:nat, (le n m) -> (le (f n) (f m)).

Lemma le_x_Sx: forall x:nat, (le x (S x)).
Proof.
intros x.
apply (le_S x).
apply (le_n x).
Qed.

(* La flecha es una abreviatura de forall cuando
el tipo dependiente no es explicito. *)
Lemma le_x_Sx_2: forall x:nat, (le x (S x)).
Proof.
intros x.
apply (le_S _ _ (le_n x)).
Qed.

Lemma le_x_SSx: forall x:nat, (le x (S (S x))).
Proof.
intro x.
apply (le_S x (S x)).
apply (le_S x x).
apply (le_n x).
Qed.

Theorem T1: forall a:nat, exists b:nat, (le (f a) b).
Proof.
intro a.
exists (S (S (S (S (S (f a)))))).
apply (le_S (f a) (S (S (S (S (f a)))))).
apply (le_S (f a) (S (S (S (f a))))).
apply (le_S (f a) (S (S (f a)))).
apply (le_S (f a) (S (f a))).
apply (le_S (f a) (f a)).
apply (le_n (f a)).
Qed.

Theorem T1_2: forall a:nat, exists b:nat, (le (f a) b).
Proof.
intro a.
exists (S (S (S (S (S (f a)))))).
repeat apply le_S.
apply (le_n (f a)).
Qed.

Theorem T1_3: forall a:nat, exists b:nat, (le (f a) b).
Proof.
intro a.
exists (S (S (S (S (S (f a)))))).
do 5 (apply le_S).
apply (le_n (f a)).
Qed.

End Ejercicio6.

Section Ejercicio7.

Variable U   : Set.
Variable A B : U -> Prop.

Theorem e71: (forall x:U, ((A x) /\ (B x)))
                       -> (forall x:U, (A x)) /\ (forall x:U, (B x)).
Proof.
intro a_x_and_b_x.
split; apply a_x_and_b_x.
Qed.

Theorem e72: (exists x:U, (A x \/ B x))->(exists x:U, A x )\/(exists x:U, B x).
Proof.
intro e_a_x_or_b_x; elim e_a_x_or_b_x.
intros x a_x_or_b_x; elim a_x_or_b_x.
- intro a_x.
  left.
  exists x.
  assumption.
- intro b_x.
  right.
  exists x.
  assumption.
Qed.

Theorem e73: (forall x:U, A x) \/ (forall y:U, B y) -> forall z:U, A z \/ B z.
Proof.
intro f_a_x_or_f_b_y; elim f_a_x_or_f_b_y.
- intros f_a_x x.
  left.
  apply f_a_x.
- intros f_b_y y.
  right.
  apply f_b_y.
Qed.

End Ejercicio7.

Section Ejercicio8.

Variables U : Set.
Variables T V : U -> Prop.
Variables R : U -> U -> Prop.

Theorem e81: (exists y : U, forall x : U, R x y) -> forall x : U, exists y : U, R x y.
Proof.
intros e_y_f_x_r_x_y x; elim e_y_f_x_r_x_y.
intros y f_r_x_y.
exists y.
apply (f_r_x_y x).
Qed.

Theorem e82:
  (exists x:U, True) /\ (forall x:U, (T x) \/ (V x))
    -> (exists z:U, (T z)) \/ (exists w:U, (V w)).
Proof.
intros true_and_f_t_x_or_v_x; elim true_and_f_t_x_or_v_x.
intros true f_t_x_or_v_x.
elim true; intros u t. (* Esto es clave *)
elim (f_t_x_or_v_x u).
- intro t_u.
  left.
  exists u.
  exact t_u.
- intro v_u.
  right.
  exists u.
  exact v_u.
Qed.

(* 
Parte 8.3. La proposición (exists x:U, True) ...
U tiene que ser distinto de vacio.
*)

End Ejercicio8.

Section Ejercicio9.
Require Import Classical.
Variables U : Set.
Variables A : U -> Prop.

Lemma not_ex_not_forall: (~exists x :U, ~A x) -> (forall x:U, A x).
Proof.
intros e_not_a_x x.
elim (classic (A x)).
- intro; assumption.
- intro not_a_x.
  elim e_not_a_x.
  exists x.
  exact not_a_x.
Qed.

Lemma not_forall_ex_not: (~forall x :U, A x) -> (exists x:U,  ~A x).
Proof.
intros not_f_a_x.
apply NNPP.
intros h.
apply not_f_a_x.
apply not_ex_not_forall. (* Idea de Katy, está loca *)
exact h.
Qed.

End Ejercicio9.

Section Ejercicio10.

Variable nat : Set. (* Variable es local *)
Variable  O  : nat.
Variable  S  : nat -> nat.

Axiom disc   : forall n:nat, ~O=(S n).
Axiom inj    : forall n m:nat, (S n)=(S m) -> n=m.
Axiom allNat : forall n: nat, n = O \/ exists m: nat, S m = n.

Variable sum prod : nat->nat->nat.

Axiom sum0   : forall n :nat, (sum n O)=n.
Axiom sumS   : forall n m :nat, (sum n (S m))=(S (sum n m)).
Axiom prod0  : forall n :nat, (prod n O)=O.
Axiom prodS  : forall n m :nat, (prod n (S m))=(sum n (prod n m)).

Lemma L10_1: (sum (S O) (S O)) = (S (S O)).
Proof.
transitivity (S (sum (S O) O)).
- apply sumS.
- replace (sum (S O) O) with (S O).
    * reflexivity.
    * symmetry.
      apply sum0.
Qed.

Lemma L10_2: forall n :nat, ~(O=n /\ (exists m :nat, n = (S m))).
Proof.
intros n abs; elim abs; intros n0 not_n0.
absurd (O = n).
(* n positivo *)
- elim not_n0; intros x n_pos.
  replace n with (S x).
  apply disc.
(* n 0 *)
- exact n0.
Qed.

Lemma prod_neutro: forall n :nat, (prod n (S O)) = n.
Proof.
intro x.
replace (prod x (S O)) with (sum x (prod x O)).
replace (prod x O) with O.
apply sum0.
symmetry; apply prod0.
symmetry; apply prodS.
Qed.

Lemma diff: forall n:nat, ~(S (S n))=(S O).
Proof.
intros x.
unfold not.
intros abs.
apply (disc x).
symmetry.
apply inj.
exact abs.
Qed.

Lemma L10_3: forall n: nat, exists m: nat, prod n (S m) = sum n n. 
(* Proof.
intros x.
exists (S O).
replace (prod x (S (S O))) with (sum x (prod x (S O))).
replace (sum x (prod x (S O))) with (sum x (sum x (prod x O))).
replace (sum x (sum x (prod x O))) with (sum x (sum x O)).
replace (sum x O) with x.
- reflexivity.
- symmetry; apply sum0.
- replace (prod x O) with O.
  * reflexivity.
  * symmetry; apply prod0.
- replace (prod x (S O)) with (sum x (prod x O)).
  * reflexivity.
  * symmetry; apply prodS.
- symmetry; apply prodS.
Qed. *)
Proof.
intro n.
exists (S O).
rewrite prodS.
rewrite prodS.
rewrite prod0.
rewrite sum0.
reflexivity.
Qed.

Lemma L10_4: forall m n: nat, n <> O -> sum m n <> O.  
Proof.
intros x y; unfold not; intros y_neq_0 x_plus_y__eq_0.
elim (allNat y).
- (* y = 0 *)
  intros y_eq_0.
  absurd (y = O).
  exact y_neq_0.
  exact y_eq_0.
- (* y =/= 0 *)
  intros e_y_neq_0.
  elim e_y_neq_0; intros y' suc_y.
  rewrite <- suc_y in x_plus_y__eq_0.
  rewrite sumS in x_plus_y__eq_0.
  apply (disc (sum x y')).
  symmetry.
  exact x_plus_y__eq_0.
Qed.

Lemma L10_5: forall m n: nat, sum m n = O -> m = O /\ n = O.  
Proof.
intros x y sum_x_y_eq_0.
elim (allNat y); elim (allNat x); intros; split; try assumption.
- (* x =/= 0 y = 0 *)
  rewrite H0 in sum_x_y_eq_0.
  rewrite sum0 in sum_x_y_eq_0.
  exact sum_x_y_eq_0.
- (* x = 0 y =/= 0 *)
  elim H0; intros y' suc_y'.
  rewrite <- suc_y' in sum_x_y_eq_0.
  rewrite sumS in sum_x_y_eq_0.
  elim (disc (sum x y')).
  symmetry.
  exact sum_x_y_eq_0.
- (* x =/= 0 y =/= 0 *)
  elim H0; intros y' suc_y'.
  rewrite <- suc_y' in sum_x_y_eq_0.
  rewrite sumS in sum_x_y_eq_0.
  elim (disc (sum x y')).
  symmetry.
  exact sum_x_y_eq_0.
- (* x =/= 0 y =/= 0 *)
  elim H0; intros y' suc_y'.
  rewrite <- suc_y' in sum_x_y_eq_0.
  rewrite sumS in sum_x_y_eq_0.
  elim (disc (sum x y')).
  symmetry.
  exact sum_x_y_eq_0.
Qed.

Lemma lemma_aux: forall n m: nat, ~(sum (S n) m = O).
Proof.
intros x y.
elim (allNat y).
- (* y = 0 *)
  intros y_eq_0.
  rewrite y_eq_0.
  rewrite sum0.
  intro s_x_eq_0.
  elim (disc x).
  symmetry.
  exact s_x_eq_0.
- (* y =/= 0 *)
  intros e_y_neq_0.
  elim (e_y_neq_0); intros y' suc_y'.
  rewrite <- suc_y'.
  rewrite sumS.
  intro abs.
  elim (disc (sum (S x) y')).
  symmetry.
  exact abs.
Qed.

Lemma L10_6: forall m n: nat, prod m n = O -> m = O \/ n = O.  
Proof.
intros x y prod_x_y_eq_0.
elim (allNat y); elim (allNat x).
- intros x_eq_0 y_eq_0.
  left.
  exact x_eq_0.
- intros x_neq_0 y_eq_0.
  right.
  exact y_eq_0.
- intros x_eq_0 y_neq_0.
  left.
  exact x_eq_0.
- intros x_neq_0 y_neq_0.
  right. (* prod definido sobre el segundo argumento *)
  elim (x_neq_0); intros x' suc_x'.
  elim (y_neq_0); intros y' suc_y'.
  rewrite <- suc_x' in prod_x_y_eq_0.
  rewrite <- suc_y' in prod_x_y_eq_0.
  rewrite prodS in prod_x_y_eq_0.
  elim (lemma_aux x' (prod (S x') y')).
  exact prod_x_y_eq_0.
Qed.

End Ejercicio10.

Section Ejercicio11.

Variable le : nat->nat->Prop.
Axiom leinv: forall n m:nat, (le n m) -> n=O \/
      (exists p:nat, (exists q:nat, n=(S p)/\ m=(S q) /\ (le p q))).

Lemma notle_s_o: forall n:nat, ~(le (S n) O).
Proof.
intros x le_s_n_0.
elim (leinv (S x) O).
- (* S n = 0 *)
  intros s_n_eq_0.
  apply (disc nat 0 S x).
  symmetry.
  exact s_n_eq_0.
(* Todo esto porque n esta definido localmente para el axioma *)
- (* S n =/= 0 *)
  intros e; elim e. intros x' e'.
  elim e'; intros y' e''.
  elim e''; intros s_x_eq_s_x' e'''.
  elim e'''; intros O_eq_s_y le_x_y.
  apply (disc nat 0 S y').
  exact O_eq_s_y.
- exact le_s_n_0.
Qed.

End Ejercicio11.
