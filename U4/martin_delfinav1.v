Section Ejercicio1.

Inductive list (A: Set): Set :=
  | nil: list A
  | cons: A -> list A -> list A.

Inductive binTree (A: Set): Set :=
  | empty_bt: binTree A
  | add_bt: A -> binTree A -> binTree A -> binTree A.
(* Información en los nodos internos *)

Inductive array (A:Set): nat -> Set :=
  | empty_a : array A 0
  | add_a : forall n:nat, A -> array A n -> array A (S n).

Inductive matrix (A:Set): nat -> nat -> Set :=
  | one_col : forall n:nat, array A n -> matrix A 1 n
  | extend_col : forall m n:nat, matrix A m n -> array A n -> matrix A (S m) n.

Inductive leq: nat -> nat -> Prop :=
  | leq0 : forall n:nat, leq 0 n
  | leqS : forall n m:nat, leq n m -> leq (S n) (S m).

Inductive leq_2: nat -> nat -> Prop :=
  | leq0_2 : forall n:nat, leq_2 n n
  | leqS_2 : forall n m:nat, leq n m -> leq_2 n (S m).

Inductive eq_list (A: Set): list A -> list A -> Prop :=
  | eq_list_nil: eq_list _ (nil _) (nil _)
  | eq_list_cons: forall (a: A) l l',
      eq_list _ l l' -> eq_list _ (cons _ a l) (cons _ a l').

(* Set implicit arguments *)
(* ( (A:=A)  para explicitar ) *)

Parameter R: forall (A: Set), A -> A -> Prop.
Inductive sorted (A: Set) R: list A -> Prop :=
  | sorted_nil: sorted A R (nil A)
  | sorted_one: forall (a:A), sorted A R (cons A a (nil A))
  | sorted_cons: forall (a b: A) l,
      R A a b -> sorted A R (cons A b l) ->
        sorted A R (cons A a (cons A b l)).

Inductive mirror (A: Set): binTree A -> binTree A -> Prop :=
  | mirror_nil: mirror A (empty_bt A) (empty_bt A)
  | mirror_bt: forall (a:A) l1 r1 l2 r2,
      mirror _ l1 r2 -> mirror _ r1 l2 ->
        mirror _ (add_bt _ a l1 r1) (add_bt _ a l2 r2).

Inductive isomorfo (A: Set): binTree A -> binTree A -> Prop :=
  | isomorfo_nil: isomorfo A (empty_bt A) (empty_bt A)
  | isomorfo_bt: forall (a b:A) l1 r1 l2 r2,
      isomorfo _ l1 l2 -> isomorfo _ r1 r2 ->
        isomorfo _ (add_bt _ a l1 r1) (add_bt _ b l2 r2).

Inductive gtree (A: Set): Set :=
  | node_gtree: A -> forest A -> gtree A
with
  forest (A: Set): Set :=
    | empty_f: forest A
    | add_f: gtree A -> forest A -> forest A.

End Ejercicio1.

Section Ejercicio2.

Definition bool_not(b: bool): bool :=
  match b with
      | true => false
      | false => true
  end.

Definition bool_xor(b1 b2: bool): bool :=
  match b1, b2 with
      | true, false => true
      | false, true => true
      | _, _ => false
  end.

Definition bool_or(b1 b2: bool): bool :=
  match b1, b2 with
      | true, _ => true
      | _, true => true
      | false, false => false
  end.

Definition bool_and(b1 b2: bool): bool :=
  match b1, b2 with
      | false, _ => false
      | _, false => false
      | _, _ => true
  end.

Definition is_nil (a: Set) (l: list a): bool :=
  match l with
      | nil _ => true
      | cons _ _ _ => false
  end.

End Ejercicio2.

Section Ejercicio3.

(* sum: nat -> nat -> nat. *)
Fixpoint suml (n m: nat) {struct n}: nat :=
  match n with
    | 0 => m
    | S n' => S (suml n' m)
  end.

(* {struct m} argumento sobre el que se hace induccion *)
Fixpoint sumr (n m: nat) {struct m}: nat :=
  match m with
    | 0 => n
    | S m' => S (sumr n m')
  end.

Fixpoint prod (n m: nat) {struct n}: nat :=
  match n with
    | 0 => 0
    | S n' => suml (prod n' m) m
  end.

Fixpoint pot (n m: nat) {struct n}: nat :=
  match n with
    | 0 => 1
    | S n' => prod (pot n' m) m
  end.

Fixpoint le_bool (n m: nat) {struct n}: bool :=
  match n with
    | 0 => true
    | S n' => match m with
        | 0 => false
        | S m' => le_bool n' m'
        end
  end.

End Ejercicio3.

Section Ejercicio4.

Fixpoint length (a: Set) (l: list a) {struct l}: nat :=
  match l with
    | nil _ => 0
    | cons _ _ l' => S (length a l')
  end.
(* Print length. *)

Fixpoint append (a: Set) (l1 l2: list a) {struct l1}: list a :=
  match l1 with
    | nil _ => l2
    | cons _ x l1' => cons _ x (append _ l1' l2)
  end.
(* Print append. *)

Fixpoint reverse (a: Set) (l: list a) {struct l}: list a :=
  match l with
    | nil _ => nil _
    | cons _ x l' => append _ (reverse _ l') (cons _ x (nil _))
  end.
(* Print reverse. *)

Fixpoint filter (a: Set) (p: a -> bool) (l: list a) {struct l}: list a :=
  match l with
    | nil _ => nil _
    | cons _ x l' =>
  if (p x) then (cons _ x (filter _ p l'))
           else filter _ p l'
  end.
(* Print filter. *)

Fixpoint map (a b: Set) (f: a -> b) (l: list a) {struct l}: list b :=
  match l with
    | nil _ => nil _
    | cons _ x l' => cons _ (f x) (map _ _ f l')
  end.
(* Print map. *)

Fixpoint exists_ (a: Set) (p: a -> bool) (l: list a) {struct l}: bool :=
  match l with
    | nil _ => false
    | cons _ x l' =>
  if (p x) then true
           else exists_ _ p l'
  end.
(* Print exists_. *)

Fixpoint exists_2 (a: Set) (p: a -> bool) (l: list a) {struct l}: bool :=
  match l with
    | nil _ => false
    | cons _ x l' => bool_or (p x) (exists_2 _ p l')
  end.

End Ejercicio4.

Section Ejercicio5.

Fixpoint inverse (a: Set) (t: binTree a) {struct t}: binTree a :=
  match t with
    | empty_bt _ => empty_bt _
    | add_bt _ x l r =>
        add_bt _ x (inverse _ r) (inverse _ l)
  end.
(* Print inverse. *)

Fixpoint int_tree_size (a: Set) (t: gtree a): nat :=
  match t with | (node_gtree _ _ f) => S (int_forest_size  _ f)
  end
with int_forest_size (a: Set) (f: forest a): nat :=
  match f with
    | empty_f _ => 0
    | add_f _ t x => plus (int_tree_size _ t) (int_forest_size _ x)
  end.

Fixpoint ext_tree_size (a: Set) (t: gtree a) : nat :=
  match t with | (node_gtree _ _ f) => ext_forest_size _ f
  end
with ext_forest_size (a: Set) (f: forest a): nat :=
  match f with
    | empty_f _ => 1
    | add_f _ t x => plus (ext_tree_size _ t) (ext_forest_size _ x)
  end.

Definition ext_le_int (a: Set) (t: gtree a): bool :=
  Nat.leb (ext_tree_size _ t) (int_tree_size _ t).

End Ejercicio5.

Section Ejercicio6.

Definition listN:= list nat.

Fixpoint member (l: listN) (n: nat) {struct l}: bool :=
  match l with
    | nil _ => false
    | cons _ x l' => if Nat.eqb x n
        then true
        else member l' n
  end.

Fixpoint delete (l: listN) (n: nat) {struct l}: list nat :=
  match l with
    | nil _ => nil _
    | cons _ x l' => if Nat.eqb x n
        then delete l' n
        else cons _ x (delete l' n)
  end.

Fixpoint insert_sort (l: listN) (n: nat) {struct l}: list nat :=
  match l with
    | nil _ => cons _ n (nil _)
    | cons _ x l' => if le_bool x n
        then cons _ x (insert_sort l' n)
        else cons _ n (insert_sort l' x)
  end.

Fixpoint insertion_sort (l: list nat) {struct l}: list nat :=
  match l with
    | nil _ => nil _
    | cons _ x l' => insert_sort (insertion_sort l') x
  end.

Fixpoint member_g (a: Set) (l: list a) (y: a) (eq: a -> a -> bool) {struct l}: bool :=
  match l with
    | nil _ => false
    | cons _ x l' => if eq x y
        then true
        else member_g _ l' y eq
  end.

Fixpoint delete_g (a: Set) (l: list a) (n: a) (eq: a -> a -> bool) {struct l}: list a :=
  match l with
    | nil _ => nil _
    | cons _ x l' => if eq x n
        then delete_g _ l' n eq
        else cons _ x (delete_g _ l' n eq)
  end.

Fixpoint insert_sort_g (a: Set) (l: list a) (n: a) (leq: a -> a -> bool) {struct l}: list a :=
  match l with
    | nil _ => cons _ n (nil _)
    | cons _ x l' => if leq x n
        then cons _ x (insert_sort_g _ l' n leq)
        else cons _ n (insert_sort_g _ l' x leq)
  end.

Fixpoint insertion_sort_g (a: Set) (l: list a) (leq: a -> a -> bool) {struct l}: list a :=
  match l with
    | nil _ => nil _
    | cons _ x l' => insert_sort_g _ (insertion_sort_g _ l' leq) x leq
  end.

End Ejercicio6.

Section Ejercicio7.

Inductive Exp (a: Set): Set :=
  | const: a -> Exp a
  | sum_exp: Exp a -> Exp a -> Exp a
  | prod_exp: Exp a -> Exp a -> Exp a
  | op_exp: Exp a -> Exp a.

(* intexp ::= - intexp
  | intexp + intexp
  | intexp * intexp *)

Fixpoint eval_nat (e: Exp nat): nat :=
  match e with
    | const _ a => a
    | sum_exp _ a b => (eval_nat a) + (eval_nat b)
    | prod_exp _ a b => (eval_nat a) * (eval_nat b)
    | op_exp _ a => eval_nat a
  end.

Fixpoint eval_bool (e: Exp bool): bool :=
  match e with
    | const _ a => a
    | sum_exp _ a b => bool_or (eval_bool a) (eval_bool b)
    | prod_exp _ a b => bool_and (eval_bool a) (eval_bool b)
    | op_exp _ a => bool_not (eval_bool a)
  end.

End Ejercicio7.

Section Ejercicio8.

Lemma and_assoc: forall a b: bool, bool_and a b = bool_and b a.
Proof.
intros x y.
case x; case y; compute; reflexivity.
Qed.

Lemma or_assoc: forall a b: bool, bool_or a b = bool_or b a.
Proof.
intros x y.
case x; case y; compute; reflexivity.
Qed.

Lemma LAnd: forall a b: bool, bool_and a b = true <-> a = true /\ b = true.
Proof.
intros x y.
unfold iff; split; intro h.
- destruct x; destruct y; simpl in h; split; try reflexivity; try discriminate.
- elim h; intros x_eq_true y_eq_true.
  rewrite x_eq_true; rewrite y_eq_true.
  compute; reflexivity.
Qed.

Lemma LOr1: forall a b: bool, bool_or a b = false <-> a = false /\ b = false.
Proof.
intros x y.
unfold iff; split; intro h.
- destruct x; destruct y; simpl in h; split; try reflexivity; try discriminate.
- elim h; intros x_eq_false y_eq_false.
  rewrite x_eq_false; rewrite y_eq_false.
  compute; reflexivity.
Qed.

Lemma LOr2: forall a b: bool, bool_or a b = true <-> a = true \/ b = true.
Proof.
intros x y.
unfold iff; split; intro h.
- destruct x; simpl in h.
  * left. reflexivity.
  * destruct y.
    + right. reflexivity.
    + left. assumption.
- elim h; intros; rewrite H; compute.
  * reflexivity.
  * case x; reflexivity.
Qed.

Lemma LXor: forall a b: bool, bool_xor a b = true <-> a <> b.
Proof.
intros x y.
unfold iff; split; intro h.
- destruct x; destruct y; discriminate.
- destruct x; destruct y; compute; try reflexivity; unfold not in h; elim h; reflexivity.
Qed.

Lemma LNot: forall b: bool, bool_not (bool_not b) = b.
Proof.
destruct b; compute; reflexivity.
Qed.

End Ejercicio8.

Section Ejercicio9.

Lemma SumO: forall n: nat, suml n 0 = n /\ suml 0 n = n.
Proof.
induction n.
- split; compute; reflexivity.
- split; elim IHn; intros hi1 hi2; simpl.
  * rewrite hi1.
    reflexivity.
  * reflexivity.
Qed.

Lemma SumS: forall n m: nat, suml n (S m) = suml (S n) m.
Proof.
induction n.
- intro x; simpl; reflexivity.
- intro x.
  simpl.
  rewrite (IHn x).
  simpl.
  reflexivity.
Qed.

Lemma SumAsoc: forall n m p : nat, suml n (suml m p) = suml (suml n m) p.
Proof.
intros x y z.
induction x.
- simpl; reflexivity.
- simpl. elim IHx; reflexivity.
Qed.

Lemma SumConm: forall n m: nat, suml n m = suml m n.
Proof.
intros x y.
induction x; induction y.
- reflexivity.
- simpl. elim IHy. simpl. reflexivity.
- simpl. rewrite IHx. simpl. reflexivity.
- simpl. rewrite IHx.
  rewrite (SumS y x).
  reflexivity.
Qed.

End Ejercicio9.