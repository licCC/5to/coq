Section Ejercicio1.

Inductive list (A: Set): Set :=
  | nil: list A
  | cons: A -> list A -> list A.

Inductive binTree (A: Set): Set :=
  | empty_bt: binTree A
  | add_bt: A -> binTree A -> binTree A -> binTree A.
(* Información en los nodos internos *)

Inductive array (A:Set): nat -> Set :=
  | empty_a : array A 0
  | add_a : forall n:nat, A -> array A n -> array A (S n).

Inductive matrix (A:Set): nat -> nat -> Set :=
  | one_col : forall n:nat, array A n -> matrix A 1 n
  | extend_col : forall m n:nat, matrix A m n -> array A n -> matrix A (S m) n.

Inductive leq: nat -> nat -> Prop :=
  | leq0 : forall n:nat, leq 0 n
  | leqS : forall n m:nat, leq n m -> leq (S n) (S m).

Inductive leq_2: nat -> nat -> Prop :=
  | leq0_2 : forall n:nat, leq_2 n n
  | leqS_2 : forall n m:nat, leq n m -> leq_2 n (S m).

Inductive eq_list (A: Set): list A -> list A -> Prop :=
  | eq_list_nil: eq_list _ (nil _) (nil _)
  | eq_list_cons: forall (a: A) l l',
      eq_list _ l l' -> eq_list _ (cons _ a l) (cons _ a l').

(* Set implicit arguments *)
(* ( (A:=A)  para explicitar ) *)

Parameter R: forall (A: Set), A -> A -> Prop.
Inductive sorted (A: Set) R: list A -> Prop :=
  | sorted_nil: sorted A R (nil A)
  | sorted_one: forall (a:A), sorted A R (cons A a (nil A))
  | sorted_cons: forall (a b: A) l,
      R A a b -> sorted A R (cons A b l) ->
        sorted A R (cons A a (cons A b l)).

Inductive mirror (A: Set): binTree A -> binTree A -> Prop :=
  | mirror_nil: mirror A (empty_bt A) (empty_bt A)
  | mirror_bt: forall (a:A) l1 r1 l2 r2,
      mirror _ l1 r2 -> mirror _ r1 l2 ->
        mirror _ (add_bt _ a l1 r1) (add_bt _ a l2 r2).

Inductive isomorfo (A: Set): binTree A -> binTree A -> Prop :=
  | isomorfo_nil: isomorfo A (empty_bt A) (empty_bt A)
  | isomorfo_bt: forall (a b:A) l1 r1 l2 r2,
      isomorfo _ l1 l2 -> isomorfo _ r1 r2 ->
        isomorfo _ (add_bt _ a l1 r1) (add_bt _ b l2 r2).

Inductive gtree (A: Set): Set :=
  | node_gtree: A -> forest A -> gtree A
with
  forest (A: Set): Set :=
    | empty_f: forest A
    | add_f: gtree A -> forest A -> forest A.

End Ejercicio1.

Section Ejercicio2.

Definition bool_not(b: bool): bool :=
  match b with
      | true => false
      | false => true
  end.

Definition bool_xor(b1 b2: bool): bool :=
  match b1, b2 with
      | true, false => true
      | false, true => true
      | _, _ => false
  end.

Definition bool_or(b1 b2: bool): bool :=
  match b1, b2 with
      | true, _ => true
      | _, true => true
      | false, false => false
  end.

Definition bool_and(b1 b2: bool): bool :=
  match b1, b2 with
      | false, _ => false
      | _, false => false
      | _, _ => true
  end.

Definition is_nil (a: Set) (l: list a): bool :=
  match l with
      | nil _ => true
      | cons _ _ _ => false
  end.

End Ejercicio2.

Section Ejercicio3.

(* sum: nat -> nat -> nat. *)
Fixpoint suml (n m: nat) {struct n}: nat :=
  match n with
    | 0 => m
    | S n' => S (suml n' m)
  end.

(* {struct m} argumento sobre el que se hace induccion *)
Fixpoint sumr (n m: nat) {struct m}: nat :=
  match m with
    | 0 => n
    | S m' => S (sumr n m')
  end.

Fixpoint prod (n m: nat) {struct n}: nat :=
  match n with
    | 0 => 0
    | S n' => suml (prod n' m) m
  end.

Fixpoint pot (n m: nat) {struct m}: nat :=
  match m with
    | 0 => 1
    | S m' => prod (pot n m') n
  end.

Fixpoint le_bool (n m: nat) {struct n}: bool :=
  match n with
    | 0 => true
    | S n' => match m with
        | 0 => false
        | S m' => le_bool n' m'
        end
  end.

End Ejercicio3.

Section Ejercicio4.

Fixpoint llength (a: Set) (l: list a) {struct l}: nat :=
  match l with
    | nil _ => 0
    | cons _ _ l' => S (llength a l')
  end.
(* Print llength. *)

Fixpoint append (a: Set) (l1 l2: list a) {struct l1}: list a :=
  match l1 with
    | nil _ => l2
    | cons _ x l1' => cons _ x (append _ l1' l2)
  end.
(* Print append. *)

Fixpoint reverse (a: Set) (l: list a) {struct l}: list a :=
  match l with
    | nil _ => nil _
    | cons _ x l' => append _ (reverse _ l') (cons _ x (nil _))
  end.
(* Print reverse. *)

Fixpoint lfilter (a: Set) (p: a -> bool) (l: list a) {struct l}: list a :=
  match l with
    | nil _ => nil _
    | cons _ x l' =>
    match (p x) with
      | true => (cons _ x (lfilter _ p l'))
      | false => lfilter _ p l'
    end
  end.
(* Print lfilter. *)

Fixpoint lmap (a b: Set) (f: a -> b) (l: list a) {struct l}: list b :=
  match l with
    | nil _ => nil _
    | cons _ x l' => cons _ (f x) (lmap _ _ f l')
  end.
(* Print lmap. *)

Fixpoint exists_ (a: Set) (p: a -> bool) (l: list a) {struct l}: bool :=
  match l with
    | nil _ => false
    | cons _ x l' =>
  if (p x) then true
           else exists_ _ p l'
  end.
(* Print exists_. *)

Fixpoint exists_2 (a: Set) (p: a -> bool) (l: list a) {struct l}: bool :=
  match l with
    | nil _ => false
    | cons _ x l' => bool_or (p x) (exists_2 _ p l')
  end.

End Ejercicio4.

Section Ejercicio8.

Lemma and_assoc: forall a b: bool, bool_and a b = bool_and b a.
Proof.
intros x y.
case x; case y; compute; reflexivity.
Qed.

Lemma or_assoc: forall a b: bool, bool_or a b = bool_or b a.
Proof.
intros x y.
case x; case y; compute; reflexivity.
Qed.

Lemma LAnd: forall a b: bool, bool_and a b = true <-> a = true /\ b = true.
Proof.
intros x y.
unfold iff; split; intro h.
- destruct x; destruct y; simpl in h; split; try reflexivity; try discriminate.
- elim h; intros x_eq_true y_eq_true.
  rewrite x_eq_true; rewrite y_eq_true.
  compute; reflexivity.
Qed.

Lemma LOr1: forall a b: bool, bool_or a b = false <-> a = false /\ b = false.
Proof.
intros x y.
unfold iff; split; intro h.
- destruct x; destruct y; simpl in h; split; try reflexivity; try discriminate.
- elim h; intros x_eq_false y_eq_false.
  rewrite x_eq_false; rewrite y_eq_false.
  compute; reflexivity.
Qed.

Lemma LOr2: forall a b: bool, bool_or a b = true <-> a = true \/ b = true.
Proof.
intros x y.
unfold iff; split; intro h.
- destruct x; simpl in h.
  * left. reflexivity.
  * destruct y.
    + right. reflexivity.
    + left. assumption.
- elim h; intros; rewrite H; compute.
  * reflexivity.
  * case x; reflexivity.
Qed.

Lemma LXor: forall a b: bool, bool_xor a b = true <-> a <> b.
Proof.
intros x y.
unfold iff; split; intro h.
- destruct x; destruct y; discriminate.
- destruct x; destruct y; compute; try reflexivity; unfold not in h; elim h; reflexivity.
Qed.

Lemma LNot: forall b: bool, bool_not (bool_not b) = b.
Proof.
destruct b; compute; reflexivity.
Qed.

End Ejercicio8.

Section Ejercicio9.

Lemma SumO: forall n: nat, suml n 0 = n /\ suml 0 n = n.
Proof.
induction n.
- split; compute; reflexivity.
- split; elim IHn; intros hi1 hi2; simpl.
  * rewrite hi1.
    reflexivity.
  * reflexivity.
Qed.

Lemma SumS: forall n m: nat, suml n (S m) = suml (S n) m.
Proof.
induction n; intro x; simpl.
- reflexivity.
- rewrite (IHn x).
  simpl.
  reflexivity.
Qed.

Lemma SumAsoc: forall n m p : nat, suml n (suml m p) = suml (suml n m) p.
Proof.
intros x y z.
induction x.
- simpl; reflexivity.
- simpl. elim IHx; reflexivity.
Qed.

Lemma SumConm: forall n m: nat, suml n m = suml m n.
Proof.
intros x y.
induction x; induction y.
- reflexivity.
- simpl. elim IHy. simpl. reflexivity.
- simpl. rewrite IHx. simpl. reflexivity.
- simpl. rewrite IHx.
  rewrite (SumS y x).
  reflexivity.
Qed.

End Ejercicio9.

Section Ejercicio11.

Lemma L1 : forall (A : Set) (l : list A), append A l (nil A) = l.
Proof.
induction l; simpl.
- reflexivity.
- rewrite IHl. reflexivity.
Qed.

Lemma L2 : forall (A : Set) (l : list A) (a : A), ~(cons A a l) = nil A.
Proof.
induction l; intros e; discriminate.
Qed.

Lemma L3 : forall (A : Set) (l m : list A) (a : A),
cons A a (append A l m) = append A (cons A a l) m.
Proof.
induction l; intros m e; simpl.
- reflexivity.
- rewrite IHl. reflexivity.
Qed.

Lemma L4 : forall (A : Set) (l m : list A),
llength A (append A l m) = suml (llength A l) (llength A m).
Proof.
induction l; intros m; simpl.
- reflexivity.
- rewrite IHl. reflexivity.
Qed.

Lemma L5 : forall (A : Set) (l : list A), llength A (reverse A l) = llength A l.
Proof.
induction l; simpl.
- reflexivity.
- rewrite L4.
  rewrite IHl.
  simpl.
  rewrite (SumConm (llength A l) 1).
  simpl.
  reflexivity.
Qed.

End Ejercicio11.

Section Ejercicio12.

Lemma L7 : forall (A B : Set) (l m : list A) (f : A -> B),
lmap A B f (append A l m) = append B (lmap A B f l) (lmap A B f m).
Proof.
induction l; intros m f; simpl.
- reflexivity.
- rewrite IHl. reflexivity.
Qed.

Lemma L8 : forall (A : Set) (l m : list A) (P : A -> bool),
lfilter A P (append A l m) = append A (lfilter A P l) (lfilter A P m).
Proof.
induction l; intros m p; simpl.
- reflexivity.
- rewrite IHl.
  case (p a).
  * simpl. reflexivity.
  * reflexivity.
Qed.

Lemma L9 : forall (A : Set) (l : list A) (P : A -> bool),
lfilter A P (lfilter A P l) = lfilter A P l.
Proof.
induction l; intros p; simpl.
- reflexivity.
- case_eq (p a); intro p_value. (* Alternativamente a case_eq: destruct (p a) eqn:p_value *)
  * simpl. rewrite p_value. rewrite IHl. reflexivity.
  * rewrite IHl. reflexivity.
Qed.

Lemma L10 : forall (A : Set) (l m n : list A),
append A l (append A m n) = append A (append A l m) n.
Proof.
induction l; intros m n; simpl.
- reflexivity.
- rewrite IHl. reflexivity.
Qed.

End Ejercicio12.

Section Ejercicio13.

Fixpoint filterMap (A B : Set) (P : B -> bool) (f : A -> B)
(l : list A) {struct l} : list B :=
  match l with
    | nil _ => nil B
    | cons _ a l' =>
    match P (f a) with
      | true => cons _ (f a) (filterMap _ _ P f l')
      | false => filterMap _ _ P f l'
  end
end.

Lemma FusionFilterMap :
forall (A B : Set) (P : B -> bool) (f : A -> B) (l : list A),
lfilter B P (lmap A B f l) = filterMap A B P f l.
Proof.
induction l; simpl.
- reflexivity.
- case_eq (P (f a)); intro p_f_value; rewrite IHl; reflexivity.
Qed.

End Ejercicio13.

Section Ejercicio17.

Inductive posfijo (A: Set): list A -> list A -> Prop :=
  | pos_id : forall l, posfijo _ l l
  | pos_propio : forall (l l': list A) (e: A), posfijo _ l l' -> posfijo _ l (cons _ e l').

Lemma PFL1: forall (A: Set) (l1 l2 l3: list A), l2 = append _ l3 l1 -> posfijo _ l1 l2.
Proof.
intros a l1 l2 l3 h.
rewrite h. clear h. (* Sin clear h esta prueba no termina *)
induction l3; simpl.
- exact (pos_id _ l1).
- constructor.
  apply IHl3.
Qed.
(* tengo que aplicar constructor en una hipotesis para no usar clear h *)

Lemma PFL2: forall (A: Set) (l1 l2: list A), posfijo _ l1 l2 -> exists (l3: list A), l2 = append _ l3 l1.
Proof.
intros a l1 l2 h.
induction h.
- exists (nil _).
  simpl.
  reflexivity.
- elim IHh; intros l'' h'.
  rewrite h'.
  exists (cons _ e l'').
  simpl.
  reflexivity.
Qed.

Lemma PFL3: forall (A: Set) (l1 l2: list A), posfijo _ l2 (append _ l1 l2).
Proof.
intros a l1 l2.
induction l1; simpl; constructor.
  - assumption.
Qed.

(* Version menos factorizada para ver la estructura de prueba. *)
(* Lemma PFL3_2: forall (A: Set) (l1 l2: list A), posfijo _ l2 (append _ l1 l2).
Proof.
intros a l1 l2.
induction l1; simpl.
  - constructor.
  - constructor.
    assumption.
Qed. *)

Fixpoint ultimo (a: Set) (l: list a) {struct l}: list a :=
  match l with
    | nil _ => nil _
    | cons _ x l' =>
      match l' with
        | nil _ => cons _ x (nil _)
        | cons _ _ _ => ultimo _ l'
      end
  end.

Lemma ultimo_cons: forall (A: Set) (l: list A) (x y: A), ultimo _ (cons _ x (cons _ y l)) = ultimo _ (cons _ y l).
(* la lista debe tener por lo menos 2 elementos para que valga el lema *)
Proof.
intros a l x y.
induction l; simpl.
- reflexivity.
- case l.
  * reflexivity.
  * intros z l'; reflexivity.
Qed.

Lemma ultimo_posfijo: forall (A: Set) (l: list A), posfijo _ (ultimo _ l) l.
Proof.
induction l. simpl.
- constructor.
- destruct l. (* no usar case aca *)
  * constructor.
  * constructor.
    rewrite (ultimo_cons _ l a a0).
    exact IHl.
Qed.

End Ejercicio17.

Section Ejercicio20.

Inductive ComTree (A: Set): nat -> Set :=
  | leaf_com: A -> ComTree A 0
  | add_com: forall (n: nat), A -> ComTree A n -> ComTree A n -> ComTree A (S n).

Fixpoint h (n: nat) (A: Set) (t: ComTree A n) {struct t}: nat :=
  match t with
    | leaf_com _ _ => 1
    | add_com _ _ _ l r => suml (h _ _ l) (h _ _ r)
  end.
(* Alternativamente definirla por recursion en n *)

(* n^0 = 1 forall n>0 *)
Axiom potO : forall n : nat, pot (S n) 0 = 1.

(* 2^{m+1} = 2^m + 2^m *)
Axiom potS : forall m: nat, pot 2 (S m) = suml (pot 2 m) (pot 2 m).

Lemma comtsize: forall (n:nat) (A:Set) (t: ComTree A n), h _ _ t = pot 2 n.
Proof.
induction t.
- (* rewrite (potO 1). *)
  simpl.
  reflexivity.
- rewrite (potS n).
  simpl.
  rewrite IHt1.
  rewrite IHt2.
  reflexivity.
Qed.

(* Lemma t_0: forall (A:Set) (t: ComTree A 0), exists (e: A), t = leaf_com _ e.
Proof.
Admitted. *)

End Ejercicio20.

Section Ejercicio21.

Inductive AB (A: Set): nat -> Set :=
  | nil_ab: AB A 0
  | add_ab: forall (n m: nat), A -> AB A n -> AB A m -> AB A (S (max n m)).

Fixpoint camino (n: nat) (A: Set) (t: AB A n) {struct t}: list A :=
  match t with
    | nil_ab _ => nil _
    | add_ab _ n m e l r =>
      match le_bool m n with
        | true => cons _ e (camino _ _ l)
        | false => cons _ e (camino _ _ r)
      end
  end.

Lemma max_r: forall (n m : nat), le_bool n m = true -> max m n = m.
Proof.
induction n; intros; destruct m; simpl.
- reflexivity.
- reflexivity.
- discriminate.
- rewrite IHn.
  * reflexivity.
  * simpl in H. assumption.
Qed.

Lemma max_l: forall (n m : nat), le_bool n m = false -> max m n = n.
induction n; intros; destruct m; simpl.
- reflexivity.
- discriminate.
- reflexivity.
- rewrite IHn.
  * reflexivity.
  * simpl in H. assumption.
Qed.

Lemma largo_camino: forall (n: nat) (A: Set) (t: AB A n), llength _ (camino n A t) = n.
Proof.
induction t; simpl.
- reflexivity.
- case_eq (le_bool m n); intro le_value ; simpl.
  * rewrite IHt1.
    rewrite (max_r m n).
    + reflexivity.
    + assumption.
  * rewrite IHt2.
    rewrite (max_l m n).
    + reflexivity.
    + assumption.
Qed.

End Ejercicio21.