Section Ejercicio4.

Variable A : Set.

Inductive AB: Set :=
  | null : AB
  | consT: A -> AB -> AB -> AB.

Inductive pertenece (a : A) : AB -> Prop :=
  | root : forall l r: AB, pertenece a (consT a l r)
  | left_mem : forall (l r: AB) (b: A), pertenece a l -> pertenece a (consT b l r)
  | right_mem : forall (l r: AB) (b: A), pertenece a r -> pertenece a (consT b l r).

Parameter eqGen: A -> A -> bool.
Axiom equalT1 : forall x y : A, eqGen x y = true -> x = y.
Axiom equalT2 : forall x y : A, eqGen x y = false -> x <> y.
Axiom equalT3 : forall x : A, eqGen x x <> false.

Fixpoint borrar (b: A) (t: AB) {struct t}: AB:=
  match t with
    | null => null
    | consT x l r =>
      if eqGen b x
      then null
      else (consT x (borrar b l) (borrar b r))
  end.

Lemma borrarNopertenece: forall (t: AB) (a: A), ~(pertenece a (borrar a t)).
Proof.
induction t; intros.
- simpl.
  intro.
  inversion H.
- intro.
  inversion H.
  * case_eq (eqGen a0 a); intros; rewrite H0 in H1.
    + discriminate.
    + inversion H1.
      absurd (a0 = a).
      ++ apply (equalT2 a0 a). exact H0.
      ++ assumption.
  * case_eq (eqGen a0 a); intros; rewrite H2 in H0.
    + inversion H0.
    + inversion H0.
      apply (IHt1 a0).
      rewrite <- H5.
      exact H1.
  * case_eq (eqGen a0 a); intros; rewrite H2 in H0.
    + discriminate.
    + inversion H0.
      apply (IHt2 a0).
      rewrite <- H6.
      exact H1.
Qed.

Inductive SinRepetidos : AB -> Prop :=
  | trivialT: SinRepetidos null
  | no_repT: forall (a: A) (l r: AB),
      SinRepetidos l -> SinRepetidos r ->
      ~ pertenece a l -> ~ pertenece a r ->
          SinRepetidos (consT a l r).

End Ejercicio4.

Section Ejercicio5.

Definition Var := nat.

Inductive Bool_exp: Set :=
  | BEVar: Var -> Bool_exp
  | BEBool: bool -> Bool_exp
  | BEAnd: Bool_exp -> Bool_exp -> Bool_exp
  | BENot: Bool_exp -> Bool_exp.

Definition Valor := bool.
Definition Memoria := nat -> Valor.

Definition lookup (m: Memoria) (v: Var): Valor := m v.

Inductive BE_Eval: Bool_exp -> Memoria -> Valor -> Prop :=
  | evar: forall (m: Memoria) (v: Var), BE_Eval (BEVar v) m (lookup m v)
  | eboolt: forall (m: Memoria), BE_Eval (BEBool true) m true
  | eboolf: forall (m: Memoria), BE_Eval (BEBool false) m false
  | eandl: forall (e1 e2: Bool_exp) (m: Memoria),
      BE_Eval e1 m false -> BE_Eval (BEAnd e1 e2) m false
  | eandr: forall (e1 e2: Bool_exp) (m: Memoria),
      BE_Eval e2 m false -> BE_Eval (BEAnd e1 e2) m false
  | eandrl: forall (e1 e2: Bool_exp) (m: Memoria),
      BE_Eval e1 m true -> BE_Eval e2 m true
        -> BE_Eval (BEAnd e1 e2) m true
  | enott: forall (e: Bool_exp) (m: Memoria),
      BE_Eval e m true -> BE_Eval (BENot e) m false
  | enotf: forall (e: Bool_exp) (m: Memoria),
      BE_Eval e m false -> BE_Eval (BENot e) m true.

Lemma mem_true: forall (d: Memoria), ~(BE_Eval (BEBool true) d false).
Proof.
intros d. intro ABS.
inversion ABS.
Qed.

Lemma and_neutral: forall (d: Memoria) (e1 e2: Bool_exp) (w: Valor),
  BE_Eval e1 d true -> BE_Eval e2 d w -> BE_Eval (BEAnd e1 e2) d w.
Proof.
intros.
case_eq w; intros; rewrite H1 in H0.
- exact (eandrl e1 e2 d H H0).
- exact (eandr e1 e2 d H0).
Qed.

Lemma injectivity: forall (d: Memoria) (e: Bool_exp) (w1 w2: Valor),
  BE_Eval e d w1 -> BE_Eval e d w2 -> w1 = w2.
Proof.
induction e; intros.
- inversion H; inversion H0.
  reflexivity.
- inversion H; inversion H0;
  try reflexivity; try (rewrite <- H2 in H5; discriminate).
- inversion H; inversion H0;
  try reflexivity.
  * rewrite H10. rewrite H4. apply IHe1.
    ** rewrite <- H4. exact H5.
    ** rewrite <- H10. exact H8.
  * rewrite H10. rewrite H4. apply IHe2.
    ** rewrite <- H4. exact H5.
    ** rewrite <- H10. exact H11.
  * rewrite H10. rewrite H5. apply IHe1.
    ** rewrite <- H5. exact H3.
    ** rewrite <- H10. exact H11.
  * rewrite H10. rewrite H5. apply IHe2.
    ** rewrite <- H5. exact H6.
    ** rewrite <- H10. exact H11.
- inversion H; inversion H0;
  try reflexivity.
  * rewrite H8. rewrite H4. apply IHe.
    ** rewrite <- H4. exact H6.
    ** rewrite <- H8. exact H2.
  * rewrite H8. rewrite H4. apply IHe.
    ** rewrite <- H4. exact H6.
    ** rewrite <- H8. exact H2.
Qed.

Lemma de_morgan: forall (d: Memoria) (e1 e2: Bool_exp),
  BE_Eval e1 d false -> BE_Eval (BENot (BEAnd e1 e2)) d true.
Proof.
induction e1; intros; constructor; constructor; assumption.
Qed.

Fixpoint beval (d: Memoria) (e: Bool_exp) {struct e}: Valor :=
  match e with
    | BEVar v => lookup d v
    | BEBool b => b
    | BEAnd e1 e2 =>
        if beval d e1
        then beval d e2
        else false
    | BENot e =>
        if beval d e
        then false
        else true
  end.

Lemma functional_correctness: forall (d: Memoria) (e: Bool_exp),
  BE_Eval e d (beval d e).
Proof.
induction e; simpl.
- constructor.
- case b; constructor.
- case_eq (beval d e1); case_eq (beval d e2); intros;
rewrite H in IHe2; rewrite H0 in IHe1.
  * exact (eandrl e1 e2 d IHe1 IHe2).
  * exact (eandr e1 e2 d IHe2).
  * exact (eandl e1 e2 d IHe1).
  * exact (eandl e1 e2 d IHe1).
- case_eq (beval d e); intros; rewrite H in IHe.
  * exact (enott e d IHe).
  * exact (enotf e d IHe).
Qed.

End Ejercicio5.

Section Ejercicio6.

Inductive LInstr: Set :=
  | empty: LInstr
  | seq: Instr -> LInstr -> LInstr
  with Instr: Set :=
    | skip: Instr
    | var: Var -> Bool_exp -> Instr
    | ifthenelse: Bool_exp -> Instr -> Instr -> Instr
    | while: Bool_exp -> Instr -> Instr
    | repeat: nat -> Instr -> Instr
    | beginend: LInstr -> Instr.

Infix ";" := seq (at level 60, right associativity).

Variables v1 v2 aux: Var.

Definition PP :=
  beginend (var v1 (BEBool true);
            var v2 (BENot (BEVar (v1)));
            empty).

Definition swap :=
  beginend (var aux (BEVar v1);
            var v1 (BEVar v2);
            var v2 (BEVar aux);
            empty).

Definition update (m: Memoria) (v: Var) (x: Valor): Memoria :=
  fun (w: Var) => match Nat.eqb v w with
    | true => x
    | false => m w
  end.

Require Import Coq.Arith.PeanoNat.
(* Listadito de lemas para usar: *)
(* Search (_ =? _). *)

Lemma update_correctness1: forall (d: Memoria) (v: Var) (x: Valor),
  lookup (update d v x) v = x.
Proof.
intros.
unfold lookup.
unfold update.
rewrite (Nat.eqb_refl v).
reflexivity.
Qed.

Lemma update_correctness2: forall (d: Memoria) (v v': Var) (x: Valor),
  v =? v' = false -> lookup (update d v x) v' = lookup d v'.
Proof.
intros.
unfold lookup.
unfold update.
rewrite H.
reflexivity.
Qed.

End Ejercicio6.

Section Ejercicio7.

(* Importante, la precedencia no se preserva a través de secciones *)
Infix ";" := seq (at level 60, right associativity).

Inductive execute: Instr -> Memoria -> Memoria -> Prop :=
  | xskip: forall (m: Memoria), execute skip m m
  | xass: forall (m: Memoria) (c: Bool_exp) (v: Var) (w: Valor),
      BE_Eval c m w -> execute (var v c) m (update m v w)
  | xifthen: forall (m m': Memoria) (c: Bool_exp) (p1 p2: Instr),
      BE_Eval c m true -> execute p1 m m'
        -> execute (ifthenelse c p1 p2) m m'
  | xifelse: forall (m m': Memoria) (c: Bool_exp) (p1 p2: Instr),
      BE_Eval c m false -> execute p2 m m'
        -> execute (ifthenelse c p1 p2) m m'
  | xwhiletrue: forall (m m' m'': Memoria) (c: Bool_exp) (p: Instr),
      BE_Eval c m true -> execute p m m' -> execute (while c p) m' m''
        -> execute (while c p) m m''
  | xwhilefalse: forall (m: Memoria) (c: Bool_exp) (p: Instr),
      BE_Eval c m false -> execute (while c p) m m
  | xrepeat0: forall (m: Memoria) (p: Instr),
      execute (repeat 0 p) m m
  | xrepeatS: forall (m m' m'': Memoria) (n: nat) (p: Instr),
      execute p m m' -> execute (repeat n p) m' m''
        -> execute (repeat (S n) p) m m''
  | xbeginend: forall (m m': Memoria) (p: LInstr),
      executeL p m m' -> execute (beginend p) m m'
  with executeL: LInstr -> Memoria -> Memoria -> Prop :=
    | xempty: forall (m: Memoria), executeL empty m m
    | xseq: forall (m m' m'': Memoria) (i: Instr) (li: LInstr),
        execute i m m' -> executeL li m' m''
          -> executeL (seq i li) m m''.


(* 7.2 *)
Lemma ifthenelse_neq: forall (m m': Memoria) (c: Bool_exp) (etrue efalse: Instr),
  execute (ifthenelse (BENot c) etrue efalse) m m'
    -> execute (ifthenelse (BENot c) etrue efalse) m m'.
Proof.
intros.
inversion H.
inversion H5.
- exact (xifthen m m' (BENot c) etrue efalse H5 H6).
- exact (xifelse m m' (BENot c) etrue efalse H5 H6).
Qed.

(* 7.3 *)
Lemma while_false: forall (m m': Memoria) (p: Instr),
  execute (while (BEBool false) p) m m' -> m = m'.
Proof.
intros.
inversion_clear H.
inversion_clear H0.
reflexivity.
Qed.

(* 7.4 *)
Lemma while_unfold: forall (m m': Memoria) (c: Bool_exp) (p: Instr),
  execute (beginend (ifthenelse c p skip;
                     while c p;
                     empty)) m m'
    -> execute (while c p) m m'.
Proof.
intros.
inversion H.
inversion H1.
inversion H9.
inversion H6.
- apply (xwhiletrue m m'3 m').
  * exact H21.
  * rewrite <- H20 in H22. exact H22.
  * inversion H15.
    rewrite <- H20 in H12.
    rewrite H25 in H12.
    exact H12.
- inversion H15.
  inversion H22.
  rewrite H25 in H12.
  exact H12.
Qed.

Lemma ridiculamente_trivial: forall n: nat, S n = n + 1.
Proof.
induction n.
- simpl.
  reflexivity.
- simpl.
  rewrite IHn.
  reflexivity.
Qed.

(* 7.5 *)
Lemma repeat_unfold: forall (m m': Memoria) (n: nat) (p: Instr),
  execute (beginend (p;
                     repeat n p;
                     empty)) m m'
    -> execute (repeat (n+1) p) m m'.
Proof.
intros.
inversion H.
inversion H1.
inversion H9.
inversion H15.
rewrite H18 in H12.
rewrite <- (ridiculamente_trivial n).
apply (xrepeatS m m'1 m' n p H6 H12).
Qed.

(* importante poner memorias al final *)
Lemma repeat_composition: forall (n1 n2: nat) (p: Instr) (m1 m2 m3: Memoria),
  execute (repeat n1 p) m1 m2 -> execute (repeat n2 p) m2 m3
    -> execute (repeat (n1 + n2) p) m1 m3.
Proof.
induction n1; intros; simpl.
- inversion H.
  exact H0.
- inversion H.
  apply (xrepeatS m1 m' m3 (n1 + n2) p).
  * exact H3.
  * apply (IHn1 n2 p m' m2 m3 H6 H0).
Qed.

Lemma pp_mem_state: forall (m: Memoria) (v1 v2: Var),
  execute (PP v1 v2) m (update (update m v1 true) v2 false).
Proof.
intros.
apply xbeginend.
apply (xseq m (update m v1 true) (update (update m v1 true) v2 false)).
- apply xass.
  apply eboolt.
- apply (xseq (update m v1 true)
              (update (update m v1 true) v2 false)
              (update (update m v1 true) v2 false)).
  * apply xass.
    apply enott.
    rewrite <- (update_correctness1 m v1 true).

    assert ((update m v1 true) = (update m v1 (lookup (update m v1 true) v1))).
    rewrite update_correctness1. reflexivity.

    rewrite <- H.
    apply evar.
  * apply xempty.
Qed.

End Ejercicio7.






















