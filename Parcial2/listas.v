Section Extra.

Definition bool_or(b1 b2: bool): bool :=
  match b1, b2 with
      | true, _ => true
      | _, true => true
      | false, false => false
  end.

Fixpoint le_bool (n m: nat) {struct n}: bool :=
  match n with
    | 0 => true
    | S n' => match m with
        | 0 => false
        | S m' => le_bool n' m'
        end
  end.

(* sum: nat -> nat -> nat. *)
Fixpoint suml (n m: nat) {struct n}: nat :=
  match n with
    | 0 => m
    | S n' => S (suml n' m)
  end.

Fixpoint prod (n m: nat) {struct n}: nat :=
  match n with
    | 0 => 0
    | S n' => suml (prod n' m) m
  end.

End Extra.

Section Listas.

Inductive list (A: Set): Set :=
  | nil: list A
  | cons: A -> list A -> list A.

Inductive eq_list (A: Set): list A -> list A -> Prop :=
  | eq_list_nil: eq_list _ (nil _) (nil _)
  | eq_list_cons: forall (a: A) l l',
      eq_list _ l l' -> eq_list _ (cons _ a l) (cons _ a l').

Parameter R: forall (A: Set), A -> A -> Prop.
Inductive sorted (A: Set) R: list A -> Prop :=
  | sorted_nil: sorted A R (nil A)
  | sorted_singl: forall (a:A), sorted A R (cons A a (nil A))
  | sorted_cons: forall (a b: A) l,
      R A a b -> sorted A R (cons A b l) ->
        sorted A R (cons A a (cons A b l)).

Definition is_nil (a: Set) (l: list a): bool :=
  match l with
      | nil _ => true
      | cons _ _ _ => false
  end.

Fixpoint llength (a: Set) (l: list a) {struct l}: nat :=
  match l with
    | nil _ => 0
    | cons _ _ l' => S (llength a l')
  end.
(* Print llength. *)

Fixpoint append (a: Set) (l1 l2: list a) {struct l1}: list a :=
  match l1 with
    | nil _ => l2
    | cons _ x l1' => cons _ x (append _ l1' l2)
  end.
(* Print append. *)

Fixpoint reverse (a: Set) (l: list a) {struct l}: list a :=
  match l with
    | nil _ => nil _
    | cons _ x l' => append _ (reverse _ l') (cons _ x (nil _))
  end.
(* Print reverse. *)

Fixpoint lfilter (a: Set) (p: a -> bool) (l: list a) {struct l}: list a :=
  match l with
    | nil _ => nil _
    | cons _ x l' =>
    match (p x) with
      | true => (cons _ x (lfilter _ p l'))
      | false => lfilter _ p l'
    end
  end.
(* Print lfilter. *)

Fixpoint lmap (a b: Set) (f: a -> b) (l: list a) {struct l}: list b :=
  match l with
    | nil _ => nil _
    | cons _ x l' => cons _ (f x) (lmap _ _ f l')
  end.
(* Print lmap. *)

Fixpoint exists_ (a: Set) (p: a -> bool) (l: list a) {struct l}: bool :=
  match l with
    | nil _ => false
    | cons _ x l' =>
  if (p x) then true
           else exists_ _ p l'
  end.
(* Print exists_. *)

Fixpoint exists_2 (a: Set) (p: a -> bool) (l: list a) {struct l}: bool :=
  match l with
    | nil _ => false
    | cons _ x l' => bool_or (p x) (exists_2 _ p l')
  end.

Fixpoint member_g (a: Set) (l: list a) (y: a) (eq: a -> a -> bool) {struct l}: bool :=
  match l with
    | nil _ => false
    | cons _ x l' => if eq x y
        then true
        else member_g _ l' y eq
  end.

Fixpoint delete_g (a: Set) (l: list a) (n: a) (eq: a -> a -> bool) {struct l}: list a :=
  match l with
    | nil _ => nil _
    | cons _ x l' => if eq x n
        then delete_g _ l' n eq
        else cons _ x (delete_g _ l' n eq)
  end.

Fixpoint insert_sort_g (a: Set) (l: list a) (n: a) (leq: a -> a -> bool) {struct l}: list a :=
  match l with
    | nil _ => cons _ n (nil _)
    | cons _ x l' => if leq x n
        then cons _ x (insert_sort_g _ l' n leq)
        else cons _ n (insert_sort_g _ l' x leq)
  end.

Fixpoint insertion_sort_g (a: Set) (l: list a) (leq: a -> a -> bool) {struct l}: list a :=
  match l with
    | nil _ => nil _
    | cons _ x l' => insert_sort_g _ (insertion_sort_g _ l' leq) x leq
  end.

End Listas.

Section ListasDeNaturales.

Definition listN:= list nat.

Fixpoint member (l: listN) (n: nat) {struct l}: bool :=
  match l with
    | nil _ => false
    | cons _ x l' => if Nat.eqb x n
        then true
        else member l' n
  end.

Fixpoint delete (l: listN) (n: nat) {struct l}: list nat :=
  match l with
    | nil _ => nil _
    | cons _ x l' => if Nat.eqb x n
        then delete l' n
        else cons _ x (delete l' n)
  end.

Fixpoint insert_sort (l: listN) (n: nat) {struct l}: list nat :=
  match l with
    | nil _ => cons _ n (nil _)
    | cons _ x l' => if le_bool x n
        then cons _ x (insert_sort l' n)
        else cons _ n (insert_sort l' x)
  end.

Fixpoint insertion_sort (l: list nat) {struct l}: list nat :=
  match l with
    | nil _ => nil _
    | cons _ x l' => insert_sort (insertion_sort l') x
  end.

End ListasDeNaturales.

Section Pruebas.

Lemma SumO: forall n: nat, suml n 0 = n /\ suml 0 n = n.
Proof.
induction n.
- split; compute; reflexivity.
- split; elim IHn; intros hi1 hi2; simpl.
  * rewrite hi1.
    reflexivity.
  * reflexivity.
Qed.

Lemma SumS: forall n m: nat, suml n (S m) = suml (S n) m.
Proof.
induction n; intro x; simpl.
- reflexivity.
- rewrite (IHn x).
  simpl.
  reflexivity.
Qed.

Lemma SumAsoc: forall n m p : nat, suml n (suml m p) = suml (suml n m) p.
Proof.
intros x y z.
induction x.
- simpl; reflexivity.
- simpl. elim IHx; reflexivity.
Qed.

Lemma SumConm: forall n m: nat, suml n m = suml m n.
Proof.
intros x y.
induction x; induction y.
- reflexivity.
- simpl. elim IHy. simpl. reflexivity.
- simpl. rewrite IHx. simpl. reflexivity.
- simpl. rewrite IHx.
  rewrite (SumS y x).
  reflexivity.
Qed.

Lemma ProdConm: forall n m : nat, prod n m = prod m n.
Proof.
induction n; induction m.
- reflexivity.
- simpl. elim IHm. simpl. reflexivity.
- simpl. rewrite IHn. simpl. reflexivity.
- simpl. rewrite <- IHm.
  rewrite (IHn (S m)).
  simpl. rewrite (IHn m).
  rewrite <- (SumAsoc (prod m n) m (S n)).
  rewrite <- (SumAsoc (prod m n) n (S m)).
  rewrite (SumConm n (S m)).
  rewrite (SumS m n).
  reflexivity.
Qed.

Lemma ProdDistr: forall n m p: nat, prod (suml n m) p = suml (prod n p) (prod m p).
Proof.
induction n; intros m p.
- simpl. reflexivity.
- simpl. rewrite IHn.
  rewrite <- (SumAsoc (prod n p) (prod m p) p).
  rewrite <- (SumAsoc (prod n p) p (prod m p)).
  rewrite (SumConm p (prod m p)).
  reflexivity.
Qed.

Lemma ProdAsoc: forall n m p: nat, prod n (prod m p) = prod (prod n m) p.
Proof.
induction n; intros m p.
- simpl. reflexivity.
- simpl. rewrite (IHn m p).
  rewrite <- ProdDistr.
  reflexivity.
Qed.

Lemma ProdDistl: forall n m p: nat, prod n (suml m p) = suml (prod n m) (prod n p).
Proof.
induction n; intros m p.
- simpl. reflexivity.
- simpl. rewrite IHn.
  rewrite <- (SumAsoc (prod n m) (prod n p) (suml m p)).
  rewrite (SumConm m p).
  rewrite (SumAsoc (prod n p) p m).
  rewrite (SumConm (suml (prod n p) p) m).
  rewrite (SumAsoc (prod n m) m (suml (prod n p) p)).
  reflexivity.
Qed.

Lemma L1 : forall (A : Set) (l : list A), append A l (nil A) = l.
Proof.
induction l; simpl.
- reflexivity.
- rewrite IHl. reflexivity.
Qed.

Lemma L2 : forall (A : Set) (l : list A) (a : A), ~(cons A a l) = nil A.
Proof.
induction l; intros e; discriminate.
Qed.

Lemma L3 : forall (A : Set) (l m : list A) (a : A),
cons A a (append A l m) = append A (cons A a l) m.
Proof.
induction l; intros m e; simpl; reflexivity.
Qed.

Lemma L4 : forall (A : Set) (l m : list A),
llength A (append A l m) = suml (llength A l) (llength A m).
Proof.
induction l; intros m; simpl.
- reflexivity.
- rewrite IHl. reflexivity.
Qed.

Lemma L5 : forall (A : Set) (l : list A), llength A (reverse A l) = llength A l.
Proof.
induction l; simpl.
- reflexivity.
- rewrite L4.
  rewrite IHl.
  simpl.
  rewrite (SumConm (llength A l) 1).
  simpl.
  reflexivity.
Qed.

Lemma L7 : forall (A B : Set) (l m : list A) (f : A -> B),
lmap A B f (append A l m) = append B (lmap A B f l) (lmap A B f m).
Proof.
induction l; intros m f; simpl.
- reflexivity.
- rewrite IHl. reflexivity.
Qed.

Lemma L8 : forall (A : Set) (l m : list A) (P : A -> bool),
lfilter A P (append A l m) = append A (lfilter A P l) (lfilter A P m).
Proof.
induction l; intros m p; simpl.
- reflexivity.
- case (p a); rewrite IHl.
  * simpl. reflexivity.
  * reflexivity.
Qed.

Lemma L9 : forall (A : Set) (l : list A) (P : A -> bool),
lfilter A P (lfilter A P l) = lfilter A P l.
Proof.
induction l; intros p; simpl.
- reflexivity.
- case_eq (p a); intro p_value. (* Alternativamente a case_eq: destruct (p a) eqn:p_value *)
  * simpl. rewrite p_value.
    rewrite IHl. reflexivity.
  * rewrite IHl. reflexivity.
Qed.

Lemma L10 : forall (A : Set) (l m n : list A),
append A l (append A m n) = append A (append A l m) n.
Proof.
induction l; intros m n; simpl.
- reflexivity.
- rewrite IHl. reflexivity.
Qed.

Fixpoint filterMap (A B : Set) (P : B -> bool) (f : A -> B)
(l : list A) {struct l} : list B :=
  match l with
    | nil _ => nil B
    | cons _ a l' =>
    match P (f a) with
      | true => cons _ (f a) (filterMap _ _ P f l')
      | false => filterMap _ _ P f l'
  end
end.

Lemma FusionFilterMap :
forall (A B : Set) (P : B -> bool) (f : A -> B) (l : list A),
lfilter B P (lmap A B f l) = filterMap A B P f l.
Proof.
induction l; simpl.
- reflexivity.
- case_eq (P (f a)); intro p_f_value; rewrite IHl; reflexivity.
Qed.

Inductive posfijo (A: Set): list A -> list A -> Prop :=
  | pos_id : forall l, posfijo _ l l
  | pos_propio : forall (l l': list A) (e: A), posfijo _ l l' -> posfijo _ l (cons _ e l').

Inductive posfijo' (A: Set): list A -> list A -> Prop :=
  | pos_0 : forall l, posfijo' _ l (nil _)
  | pos_cons : forall (l l': list A) (e: A), posfijo' _ l l' ->
      posfijo' _ (cons _ e l) (cons _ e l').

Lemma PFL1: forall (A: Set) (l1 l2 l3: list A), l2 = append _ l3 l1 -> posfijo _ l1 l2.
Proof.
intros a l1 l2 l3 h.
rewrite h. clear h. (* Sin clear h esta prueba no termina *)
induction l3; simpl.
- exact (pos_id _ l1).
- constructor.
  exact IHl3.
Qed.

Lemma PFL2: forall (A: Set) (l1 l2: list A), posfijo _ l1 l2 -> exists (l3: list A), l2 = append _ l3 l1.
Proof.
intros A l1 l2 h.
induction h.
- exists (nil _).
  simpl.
  reflexivity.
- elim IHh; intros l'' h'.
  rewrite h'.
  exists (cons _ e l'').
  simpl.
  reflexivity.
Qed.

Lemma PFL3: forall (A: Set) (l1 l2: list A), posfijo _ l2 (append _ l1 l2).
Proof.
intros a l1 l2.
induction l1; simpl; constructor.
  - assumption.
Qed.

Fixpoint ultimo (a: Set) (l: list a) {struct l}: list a :=
  match l with
    | nil _ => nil _
    | cons _ x l' =>
      match l' with
        | nil _ => cons _ x (nil _)
        | cons _ _ _ => ultimo _ l'
      end
  end.

Lemma ultimo_cons: forall (A: Set) (l: list A) (x y: A), ultimo _ (cons _ x (cons _ y l)) = ultimo _ (cons _ y l).
(* la lista debe tener por lo menos 2 elementos para que valga el lema *)
Proof.
intros a l x y.
induction l; simpl.
- reflexivity.
- case l.
  * reflexivity.
  * intros z l'; reflexivity.
Qed.

Lemma ultimo_posfijo: forall (A: Set) (l: list A), posfijo _ (ultimo _ l) l.
Proof.
induction l.
- simpl.
  constructor.
- destruct l. (* no usar case aca *)
  * constructor.
  * constructor.
    rewrite (ultimo_cons _ l a a0).
    exact IHl.
Qed.

End Pruebas.