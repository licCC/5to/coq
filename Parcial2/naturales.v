Section Extra.

Definition bool_or(b1 b2: bool): bool :=
  match b1, b2 with
      | true, _ => true
      | _, true => true
      | false, false => false
  end.

Definition bool_and(b1 b2: bool): bool :=
  match b1, b2 with
      | false, _ => false
      | _, false => false
      | _, _ => true
  end.

Definition bool_not(b: bool): bool :=
  match b with
      | true => false
      | false => true
  end.

End Extra.

Section Naturales.

Inductive leq: nat -> nat -> Prop :=
  | leq0 : forall n:nat, leq 0 n
  | leqS : forall n m:nat, leq n m -> leq (S n) (S m).

Inductive leq_2: nat -> nat -> Prop :=
  | leq0_2 : forall n:nat, leq_2 n n
  | leqS_2 : forall n m:nat, leq n m -> leq_2 n (S m).

(* sum: nat -> nat -> nat. *)
Fixpoint suml (n m: nat) {struct n}: nat :=
  match n with
    | 0 => m
    | S n' => S (suml n' m)
  end.

(* {struct m} argumento sobre el que se hace induccion *)
Fixpoint sumr (n m: nat) {struct m}: nat :=
  match m with
    | 0 => n
    | S m' => S (sumr n m')
  end.

Fixpoint prod (n m: nat) {struct n}: nat :=
  match n with
    | 0 => 0
    | S n' => suml (prod n' m) m
  end.

Fixpoint pot (n m: nat) {struct m}: nat :=
  match m with
    | 0 => 1
    | S m' => prod (pot n m') n
  end.

Fixpoint le_bool (n m: nat) {struct n}: bool :=
  match n with
    | 0 => true
    | S n' => match m with
        | 0 => false
        | S m' => le_bool n' m'
        end
  end.

End Naturales.

Section ExpresionesNaturales.

Inductive Exp (a: Set): Set :=
  | const: a -> Exp a
  | sum_exp: Exp a -> Exp a -> Exp a
  | prod_exp: Exp a -> Exp a -> Exp a
  | op_exp: Exp a -> Exp a.

(* intexp ::= - intexp
  | intexp + intexp
  | intexp * intexp *)

Fixpoint eval_nat (e: Exp nat): nat :=
  match e with
    | const _ a => a
    | sum_exp _ a b => (eval_nat a) + (eval_nat b)
    | prod_exp _ a b => (eval_nat a) * (eval_nat b)
    | op_exp _ a => eval_nat a
  end.

Fixpoint eval_bool (e: Exp bool): bool :=
  match e with
    | const _ a => a
    | sum_exp _ a b => bool_or (eval_bool a) (eval_bool b)
    | prod_exp _ a b => bool_and (eval_bool a) (eval_bool b)
    | op_exp _ a => bool_not (eval_bool a)
  end.

End ExpresionesNaturales.

Section Pruebas.

Lemma SumO: forall n: nat, suml n 0 = n /\ suml 0 n = n.
Proof.
induction n.
- split; compute; reflexivity.
- split; elim IHn; intros hi1 hi2; simpl.
  * rewrite hi1.
    reflexivity.
  * reflexivity.
Qed.

Lemma SumS: forall n m: nat, suml n (S m) = suml (S n) m.
Proof.
induction n; intro x; simpl.
- reflexivity.
- rewrite (IHn x).
  simpl.
  reflexivity.
Qed.

Lemma SumAsoc: forall n m p : nat, suml n (suml m p) = suml (suml n m) p.
Proof.
intros x y z.
induction x.
- simpl; reflexivity.
- simpl; elim IHx; reflexivity.
Qed.

Lemma SumConm: forall n m: nat, suml n m = suml m n.
Proof.
intros x y.
induction x; induction y.
- reflexivity.
- simpl. elim IHy. simpl. reflexivity.
- simpl. rewrite IHx. simpl. reflexivity.
- simpl. rewrite IHx.
  rewrite (SumS y x).
  reflexivity.
Qed.

Lemma ProdConm: forall n m : nat, prod n m = prod m n.
Proof.
induction n; induction m.
- reflexivity.
- simpl. elim IHm. simpl. reflexivity.
- simpl. rewrite IHn. simpl. reflexivity.
- simpl. rewrite <- IHm.
  rewrite (IHn (S m)).
  simpl. rewrite (IHn m).
  rewrite <- (SumAsoc (prod m n) m (S n)).
  rewrite <- (SumAsoc (prod m n) n (S m)).
  rewrite (SumConm n (S m)).
  rewrite (SumS m n).
  reflexivity.
Qed.

Lemma ProdDistr: forall n m p: nat, prod (suml n m) p = suml (prod n p) (prod m p).
Proof.
induction n; intros m p.
- simpl. reflexivity.
- simpl. rewrite IHn.
  rewrite <- (SumAsoc (prod n p) (prod m p) p).
  rewrite <- (SumAsoc (prod n p) p (prod m p)).
  rewrite (SumConm p (prod m p)).
  reflexivity.
Qed.

Lemma ProdAsoc: forall n m p: nat, prod n (prod m p) = prod (prod n m) p.
Proof.
induction n; intros m p.
- simpl. reflexivity.
- simpl. rewrite (IHn m p).
  rewrite <- ProdDistr.
  reflexivity.
Qed.

Lemma ProdDistl: forall n m p: nat, prod n (suml m p) = suml (prod n m) (prod n p).
Proof.
induction n; intros m p.
- simpl. reflexivity.
- simpl. rewrite IHn.
  rewrite <- (SumAsoc (prod n m) (prod n p) (suml m p)).
  rewrite (SumConm m p).
  rewrite (SumAsoc (prod n p) p m).
  rewrite (SumConm (suml (prod n p) p) m).
  rewrite (SumAsoc (prod n m) m (suml (prod n p) p)).
  reflexivity.
Qed.

End Pruebas.