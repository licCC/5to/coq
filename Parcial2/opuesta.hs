module Opuesta where

import qualified Prelude

type Sig a = a
  -- singleton inductive, whose constructor was exist
  
opuesta :: (([]) Prelude.Bool) -> ([]) Prelude.Bool
opuesta l =
  case l of {
   ([]) -> ([]);
   (:) x xs ->
    case x of {
     Prelude.True -> (:) Prelude.False (opuesta xs);
     Prelude.False -> (:) Prelude.True (opuesta xs)}}

l2 :: (([]) Prelude.Bool) -> (([]) Prelude.Bool)
l2 =
  opuesta

