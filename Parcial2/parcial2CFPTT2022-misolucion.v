Require Import List.
Require Import Arith.
Require Import Lia.

(* ----------- Ej1 ----------- *)

Fixpoint elim (z:nat) (l:list nat) {struct l} : list nat :=
     match l with
         | nil => nil
         | cons x xs => if (Nat.eqb z x) then elim z xs else x::(elim z xs)
     end.

Fixpoint pertenece (z:nat) (l:list nat) {struct l} : bool :=
     match l with
         | nil => false
         | cons x xs => if (Nat.eqb z x) then true else pertenece z xs
     end.
 
Lemma Ej1_3_1 : forall (l:list nat) (x:nat), pertenece x (elim x l) = false.
Proof.
induction l; intros; simpl.
- reflexivity.
- case_eq (x =? a); intros.
  * exact (IHl x).
  * simpl; rewrite H.
    exact (IHl x).
Qed.

Lemma aux2: forall (n m: nat), n <= m -> S n <= S m.
Proof.
intros; induction H.
- lia.
- transitivity (S m).
  * assumption.
  * apply le_S.
    lia.
Qed.

Lemma Ej1_3_2 : forall (l:list nat) (x:nat), length (elim x l) <= length l.
Proof.
induction l; intros; simpl.
- lia.
- case_eq (x =? a); intros.
  * constructor.
    exact (IHl x).
  * simpl.
    apply aux2.
    exact (IHl x).
Qed.

Lemma aux: forall (x y: nat), (x =? y) = true -> x = y.
Proof.
intros.
apply EqNat.beq_nat_eq_stt.
symmetry.
assumption.
Qed.

Lemma Ej1_3_3 : forall (l:list nat) (x:nat), pertenece x l = true -> l <> elim x l.
Proof.
(* induction l; intros; simpl.
- discriminate.
- case_eq (x =? a); intros.
  * 
    
unfold not; intros.
    apply (IHl x).
    **
 *)
unfold not; intros.
rewrite H0 in H.
rewrite Ej1_3_1 in H.
discriminate.
Qed.


(* ----------- Ej2 ----------- *)

Inductive sublista (A : Set) : list A -> list A -> Prop :=
  | prefijo_id : forall l : list A, sublista _ nil l
  | prefijo_cons :
      forall (a : A) (l1 l2 : list A),
      sublista _ l1 l2 -> sublista _ (a::l1) (a::l2).

Local Hint Constructors sublista.

Lemma Ej2_2_1 : forall (A : Set) (l1 l2 : list A), sublista _ l1 l2 -> length l1 <= length l2.
Proof.
intros; induction H; simpl.
- lia.
- (* Aca tambien lo resuelve lia *)
  apply (aux2 (length l1) (length l2)).
  assumption.
Qed.

Lemma aux3: forall (l1 l2: list nat) (a: nat), l1 = l2 -> a :: l1 = a :: l2.
Proof.
induction l1; intros; rewrite H; reflexivity.
Qed.

Lemma Ej2_2_2 : forall (l1 l2: list nat), sublista _ l1 l2 -> sublista _ l2 l1 -> l1 = l2.
Proof.
intros.
induction H.
- inversion H0.
  reflexivity.
- inversion H0.
  apply aux3.
  exact (IHsublista H2).
Qed.

(* ----------- Ej3 ----------- *)

Inductive perm (A:Set): (list A) -> (list A) -> Prop:=
  | p_refl: forall l:list A, perm _ l l
  | p_trans: forall l m n:list A, (perm _ l m) -> (perm _ m n) -> perm _ l n
  | p_ccons: forall (a b: A) (l:list A), 
	    perm _ (cons a (cons b l)) (cons b (cons a l))
  | p_cons: forall (a:A) (l m:list A), (perm _ l m) -> perm _ (cons a l) (cons a m).


Fixpoint swap (A:Set) (l:list A) {struct l}: list A :=
  match l with
    | nil  => nil
    | (cons a l2) =>
        match l2 with 
          | nil => l
      		| (cons b l3) => cons b (cons a (swap _ l3))
		    end
  end.

Local Hint Constructors perm.

Require Import FunInd.
Functional Scheme swap_ind := Induction for swap Sort Prop.

Lemma Ej3: forall (A:Set) (l:list A), {l2:list A | perm _ l l2}.
Proof.
intros.
exists (swap _ l).
functional induction (swap _ l).
- constructor.
- constructor.
- apply (p_trans A
                 (a :: b :: l3)
                 (a :: b :: swap _ l3)
                 (b :: a :: swap _ l3)); constructor.
  * constructor.
    assumption.
Qed.
