Section Booleanos.

Definition bool_not(b: bool): bool :=
  match b with
      | true => false
      | false => true
  end.

Definition bool_xor(b1 b2: bool): bool :=
  match b1, b2 with
      | true, false => true
      | false, true => true
      | _, _ => false
  end.

Definition bool_or(b1 b2: bool): bool :=
  match b1, b2 with
      | true, _ => true
      | _, true => true
      | false, false => false
  end.

Definition bool_and(b1 b2: bool): bool :=
  match b1, b2 with
      | false, _ => false
      | _, false => false
      | _, _ => true
  end.

End Booleanos.

Section Pruebas.

Lemma and_assoc: forall a b: bool, bool_and a b = bool_and b a.
Proof.
intros x y.
case x; case y; compute; reflexivity.
Qed.

Lemma or_assoc: forall a b: bool, bool_or a b = bool_or b a.
Proof.
intros x y.
case x; case y; compute; reflexivity.
Qed.

Lemma LAnd: forall a b: bool, bool_and a b = true <-> a = true /\ b = true.
Proof.
intros x y.
unfold iff; split; intro h.
- destruct x; destruct y; simpl in h; split; try reflexivity; try discriminate.
- elim h; intros x_eq_true y_eq_true.
  rewrite x_eq_true; rewrite y_eq_true.
  compute; reflexivity.
Qed.

Lemma LOr1: forall a b: bool, bool_or a b = false <-> a = false /\ b = false.
Proof.
intros x y.
unfold iff; split; intro h.
- destruct x; destruct y; simpl in h; split; try reflexivity; try discriminate.
- elim h; intros x_eq_false y_eq_false.
  rewrite x_eq_false; rewrite y_eq_false.
  compute; reflexivity.
Qed.

Lemma LOr2: forall a b: bool, bool_or a b = true <-> a = true \/ b = true.
Proof.
intros x y.
unfold iff; split; intro h.
- destruct x; simpl in h.
  * left. reflexivity.
  * destruct y.
    + right. reflexivity.
    + left. assumption.
- elim h; intros; rewrite H; compute.
  * reflexivity.
  * case x; reflexivity.
Qed.

Lemma LXor: forall a b: bool, bool_xor a b = true <-> a <> b.
Proof.
intros x y.
unfold iff; split; intro h.
- destruct x; destruct y; discriminate.
- destruct x; destruct y; compute; try reflexivity; unfold not in h; elim h; reflexivity.
Qed.

Lemma LNot: forall b: bool, bool_not (bool_not b) = b.
Proof.
destruct b; compute; reflexivity.
Qed.

End Pruebas.