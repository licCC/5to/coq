(*
NOMBRE COMPLETO: Delfina Martin
...
*)

Require Export List.
Require Import Lia.
Require Export Arith.

Section Problema1.

Fixpoint eliminar (y:nat) (l:list nat) {struct l} : list nat :=
  match l with
    | nil => nil
    | cons x xs =>
        if (Nat.eqb x y)
        then xs
        else x :: (eliminar y xs)
  end.

Fixpoint pertenece (y:nat) (l:list nat) {struct l} : bool :=
  match l with
    | nil => false
    | cons x xs =>
        if (Nat.eqb x y)
        then true
        else pertenece y xs
  end.

Lemma aux: forall (l1 l2: list nat) (a: nat), a :: l1 = a :: l2 -> l1 = l2.
Proof.
induction l1; intros; inversion H; reflexivity.
Qed.

Lemma L1_1: forall (A:Set) (l:list A) (x:A), l <> x::l.
Proof.
induction l; intros.
- discriminate.
- intro h.
  inversion h.
  apply (IHl x).
  assumption.
Qed.

Require Import FunInd.
Functional Scheme eliminar_ind := Induction for eliminar Sort Set.

Lemma L1_2: forall (l:list nat) (x:nat),
  pertenece x l = true -> eliminar x l <> l.
Proof.
intros l x H.
functional induction (eliminar x l).
- discriminate.
- apply L1_1.
- simpl in H.
  rewrite e0 in H.
  intro h.
  apply IHl0.
  * assumption.
  * apply (aux (eliminar x xs) xs x0 h).
Qed.

End Problema1.

Section Problema2.

Inductive distintas (A:Set) : list A -> list A -> Prop :=
  | distintas_nil: distintas _ nil nil
  | distintas_cons: forall (a b: A) (l1 l2:list A),
      distintas _ l1 l2 -> distintas _ (a :: l1) (b :: l2).

Fixpoint opuesta (l:list bool) {struct l} : list bool :=
  match l with
    | nil => nil
    | cons x xs =>
        if x
        then false :: opuesta xs
        else true :: opuesta xs
  end.

Hint Constructors distintas.

Lemma L2: forall (l1: list bool), 
  { l2: list bool | distintas bool l1 l2 }.
Proof.
intros.
exists (opuesta l1).
induction l1.
- constructor.
- simpl.
  case a; constructor; assumption.
Qed.

End Problema2.

(* Require Import Coq.extraction.ExtrHaskellBasic.
Extraction Language Haskell.
Extraction "./opuesta" L2. *)

Section Problema3.
 
Definition Var := nat.
Definition Valor := nat.

Definition Memoria := Var -> Valor.

Definition lookup (m : Memoria) (v : Var) : Valor := m v.

Definition update (m: Memoria) (v: Var) (x: Valor): Memoria :=
  fun (w: Var) => match Nat.eqb v w with
    | true => x
    | false => m w
  end.
 
Inductive Instr : Set :=
  | ass: Var -> Valor -> Instr
  | seq: Instr -> Instr -> Instr
  | ifthenelse: Var -> Valor -> Instr -> Instr -> Instr.

Inductive execute : Memoria -> Instr -> Memoria -> Prop :=
  | xass: forall (m: Memoria) (v: Var) (w: Valor),
      execute m (ass v w) (update m v w)
  | xseq: forall (m m' m'': Memoria) (i1 i2: Instr),
        execute m i1 m' -> execute m' i2 m''
          -> execute m (seq i1 i2) m''
  | xifthen: forall (m m': Memoria) (i1 i2: Instr) (v: Var) (w: Valor),
      lookup m v = w -> execute m i1 m'
      -> execute m (ifthenelse v w i1 i2) m'
  | xifelse: forall (m m': Memoria) (i1 i2: Instr) (v: Var) (w: Valor),
      lookup m v <> w -> execute m i2 m'
      -> execute m (ifthenelse v w i1 i2) m'.

Lemma L3_1: forall (m1 m2: Memoria) (v: Var) (val: Valor) (i1 i2: Instr),
  lookup m1 v <> val -> execute m1 i2 m2
      -> execute m1 (ifthenelse v val i1 i2) m2.
Proof.
intros.
apply xifelse; assumption.
Qed.

Search (_ =? _).

Lemma L3_2: forall (m1 m2 m3:Memoria) (v1 v2: Var) (val: Valor) (i1 i2: Instr),
  v2 =? v1 = false -> execute m1 (seq (ass v1 val)
                              (ass v2 (val + 1))) m2 -> execute m2 i2 m3
    -> execute m2 (ifthenelse v2 (lookup m2 v1) i1 i2) m3.
Proof.
intros.
apply xifelse.
- unfold lookup.
  inversion H0.
  inversion H7.
  inversion H5.
  unfold update.
  rewrite H.
  rewrite (Nat.eqb_refl v1).
  rewrite (Nat.eqb_refl v2).
  lia.
- assumption.
Qed.
 
End Problema3.
