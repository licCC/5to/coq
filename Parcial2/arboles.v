Section Extra.

Fixpoint suml (n m: nat) {struct n}: nat :=
  match n with
    | 0 => m
    | S n' => S (suml n' m)
  end.

Inductive list (A: Set): Set :=
  | nil: list A
  | cons: A -> list A -> list A.

Fixpoint append (a: Set) (l1 l2: list a) {struct l1}: list a :=
  match l1 with
    | nil _ => l2
    | cons _ x l1' => cons _ x (append _ l1' l2)
  end.

Fixpoint llength (a: Set) (l: list a) {struct l}: nat :=
  match l with
    | nil _ => 0
    | cons _ _ l' => S (llength a l')
  end.

Lemma SumS: forall n m: nat, suml n (S m) = suml (S n) m.
Proof.
induction n; intro x; simpl.
- reflexivity.
- rewrite (IHn x).
  simpl.
  reflexivity.
Qed.

Lemma L4 : forall (A : Set) (l m : list A),
llength A (append A l m) = suml (llength A l) (llength A m).
Proof.
induction l; intros m; simpl.
- reflexivity.
- rewrite IHl. reflexivity.
Qed.

Fixpoint prod (n m: nat) {struct n}: nat :=
  match n with
    | 0 => 0
    | S n' => suml (prod n' m) m
  end.

Fixpoint pot (n m: nat) {struct m}: nat :=
  match m with
    | 0 => 1
    | S m' => prod (pot n m') n
  end.

Fixpoint le_bool (n m: nat) {struct n}: bool :=
  match n with
    | 0 => true
    | S n' => match m with
        | 0 => false
        | S m' => le_bool n' m'
        end
  end.

Lemma max_r: forall (n m : nat), le_bool n m = true -> max m n = m.
Proof.
induction n; intros; destruct m; simpl.
- reflexivity.
- reflexivity.
- discriminate.
- rewrite IHn.
  * reflexivity.
  * simpl in H. assumption.
Qed.

Lemma max_l: forall (n m : nat), le_bool n m = false -> max m n = n.
induction n; intros; destruct m; simpl.
- reflexivity.
- discriminate.
- reflexivity.
- rewrite IHn.
  * reflexivity.
  * simpl in H. assumption.
Qed.

End Extra.

Section Binarios.

Inductive binTree (A: Set): Set :=
  | empty_bt: binTree A
  | add_bt: A -> binTree A -> binTree A -> binTree A.
(* Información en los nodos internos *)

Inductive mirror (A: Set): binTree A -> binTree A -> Prop :=
  | mirror_nil: mirror A (empty_bt A) (empty_bt A)
  | mirror_bt: forall (a:A) l1 r1 l2 r2,
      mirror _ l1 r2 -> mirror _ r1 l2 ->
        mirror _ (add_bt _ a l1 r1) (add_bt _ a l2 r2).

Inductive isomorfo (A: Set): binTree A -> binTree A -> Prop :=
  | isomorfo_nil: isomorfo A (empty_bt A) (empty_bt A)
  | isomorfo_bt: forall (a b:A) l1 r1 l2 r2,
      isomorfo _ l1 l2 -> isomorfo _ r1 r2 ->
        isomorfo _ (add_bt _ a l1 r1) (add_bt _ b l2 r2).

Inductive Tree (A: Set): Set :=
  | leaf_bt: A -> Tree A
  | add_tree: Tree A -> Tree A -> Tree A.
(* Información en las hojas *)

Fixpoint tmap (a b: Set) (f: a -> b) (t: Tree a) {struct t}: Tree b :=
  match t with
    | leaf_bt _ e => leaf_bt _ (f e)
    | add_tree _ l r => add_tree _ (tmap _ _ f l) (tmap _ _ f r)
  end.
(* Print tmap. *)

Fixpoint tsize (a: Set) (t: Tree a) {struct t}: nat :=
  match t with
    | leaf_bt _ _ => 1
    | add_tree _ l r => suml (tsize _ l) (tsize _ r)
  end.

Fixpoint tree_to_list (a: Set) (t: Tree a) {struct t}: list a :=
  match t with
    | leaf_bt _ e => cons _ e (nil _)
    | add_tree _ l r => append _ (tree_to_list _ l) (tree_to_list _ r)
  end.

Fixpoint inverse (a: Set) (t: binTree a) {struct t}: binTree a :=
  match t with
    | empty_bt _ => empty_bt _
    | add_bt _ x l r =>
        add_bt _ x (inverse _ r) (inverse _ l)
  end.

Inductive ABTree (A B: Set): Set :=
  | leaf_abt: B -> ABTree A B
  | add_abtree: A -> ABTree A B -> ABTree A B -> ABTree A B.
(* Información en nodos internos y hojas *)

Fixpoint absize_ext (A B: Set) (t: ABTree A B) {struct t}: nat :=
  match t with
    | leaf_abt _ _ _ => 1
    | add_abtree _ _ _ l r => suml (absize_ext _ _ l) (absize_ext _ _ r)
  end.

Fixpoint absize_int (A B: Set) (t: ABTree A B) {struct t}: nat :=
  match t with
    | leaf_abt _ _ _ => 0
    | add_abtree _ _ _ l r => S (suml (absize_int _ _ l) (absize_int _ _ r))
  end.

Variable A : Set.

Inductive Tree_ : Set :=
  | nullT : Tree_
  | consT : A -> Tree_ -> Tree_ -> Tree_.

Inductive subarbol: Tree_ -> Tree_ -> Prop :=
  | sub_id : forall t, subarbol t t
  | sub_propio_izq : forall (t t1 t2: Tree_) (e: A),
      subarbol t t1 -> subarbol t (consT e t1 t2)
  | sub_propio_der : forall (t t1 t2: Tree_) (e: A),
      subarbol t t2 -> subarbol t (consT e t1 t2).

End Binarios.

Section Generales.

Inductive gtree (A: Set): Set :=
  | node_gtree: A -> forest A -> gtree A
with
  forest (A: Set): Set :=
    | empty_f: forest A
    | add_f: gtree A -> forest A -> forest A.

Fixpoint int_tree_size (a: Set) (t: gtree a): nat :=
  match t with | (node_gtree _ _ f) => S (int_forest_size  _ f)
  end
with int_forest_size (a: Set) (f: forest a): nat :=
  match f with
    | empty_f _ => 0
    | add_f _ t x => plus (int_tree_size _ t) (int_forest_size _ x)
  end.

Fixpoint ext_tree_size (a: Set) (t: gtree a) : nat :=
  match t with | (node_gtree _ _ f) => ext_forest_size _ f
  end
with ext_forest_size (a: Set) (f: forest a): nat :=
  match f with
    | empty_f _ => 1
    | add_f _ t x => plus (ext_tree_size _ t) (ext_forest_size _ x)
  end.

Definition ext_le_int (a: Set) (t: gtree a): bool :=
  Nat.leb (ext_tree_size _ t) (int_tree_size _ t).

Inductive AB (A: Set): nat -> Set :=
  | nil_ab: AB A 0
  | add_ab: forall (n m: nat), A -> AB A n -> AB A m -> AB A (S (max n m)).

Fixpoint camino (n: nat) (A: Set) (t: AB A n) {struct t}: list A :=
  match t with
    | nil_ab _ => nil _
    | add_ab _ n m e l r =>
      match le_bool m n with
        | true => cons _ e (camino _ _ l)
        | false => cons _ e (camino _ _ r)
      end
  end.

End Generales.

Section Completos.

Inductive ComTree (A: Set): nat -> Set :=
  | leaf_com: A -> ComTree A 0
  | add_com: forall (n: nat), A -> ComTree A n -> ComTree A n -> ComTree A (S n).

(* Cantidad de hojas *)
Fixpoint h (n: nat) (A: Set) (t: ComTree A n) {struct t}: nat :=
  match t with
    | leaf_com _ _ => 1
    | add_com _ _ _ l r => suml (h _ _ l) (h _ _ r)
  end.
(* Alternativamente definirla por recursion en n *)

(* n^0 = 1 forall n>0 *)
Axiom potO : forall n : nat, pot (S n) 0 = 1.

(* 2^{m+1} = 2^m + 2^m *)
Axiom potS : forall m: nat, pot 2 (S m) = suml (pot 2 m) (pot 2 m).

End Completos.

Section Pruebas.

Lemma mirror_inverse: forall (A: Set) (t: binTree A), mirror A (inverse A t) t.
Proof.
induction t; simpl; constructor; [exact IHt2 | exact IHt1].
Qed.

(* Menos factorizada para que sea más legible: *)
Lemma mirror_inverse2: forall (A: Set) (t: binTree A), mirror A (inverse A t) t.
Proof.
induction t; simpl.
- constructor.
- apply (mirror_bt A a (inverse A t2) (inverse A t1) t1 t2).
  (* idem constructor. *)
  * exact IHt2.
  * exact IHt1.
Qed.

Lemma iso_id: forall (A: Set) (t: binTree A), isomorfo _ t t.
Proof.
induction t; simpl; constructor; [exact IHt1 | exact IHt2].
Qed.

Lemma iso_sim: forall (A: Set) (t u: binTree A), isomorfo _ t u -> isomorfo _ u t.
Proof.
intros A t u h.
induction h.
- exact (isomorfo_nil _).
- exact (isomorfo_bt _ b a l2 r2 l1 r1 IHh1 IHh2).
Qed.
(* Probar alternativamente con inducción sobre el árbol *)

Lemma iso_inv1: forall (A: Set) (t u: binTree A), isomorfo _ t u -> t = empty_bt _ -> u = empty_bt _.
Proof.
intros.
destruct H.
- reflexivity.
- discriminate.
Qed.

Lemma iso_inv2: forall (A: Set) (a: A) (t u l r: binTree A), isomorfo A t u -> t = (add_bt _ a l r) -> exists b l r, u = add_bt _ b l r.
Proof.
intros.
destruct H.
- discriminate.
- exists b, l2, r2.
  reflexivity.
Qed.

Lemma map_tsize: forall (a b: Set) (f: a -> b) (t: Tree a), tsize _ (tmap _ _ f t) = tsize _ t.
Proof.
induction t; simpl.
- reflexivity.
- rewrite IHt1.
  rewrite IHt2.
  reflexivity.
Qed.

Lemma tree_to_list_size: forall (a: Set) (t: Tree a), tsize _ t = llength _ (tree_to_list _ t).
Proof.
induction t; simpl.
- reflexivity.
- rewrite IHt1. rewrite IHt2.
  rewrite (L4 _ (tree_to_list a t1) (tree_to_list a t2)).
  reflexivity.
Qed.

(* Lemma sum_ext: forall (A B: Set) (a: A) (t t1 t2: ABTree A B), t = add_abtree _ _ a t1 t2 ->
  absize_ext _ _ (add_abtree _ _ a t1 t2) = absize_ext _ _ t1 + absize_ext _ _ t2.
Proof.
induction t1.
- intros.
  constructor.
- intros.
  tauto.
Qed.

Lemma sum_int: forall (A B: Set) (a: A) (t t1 t2: ABTree A B), t = add_abtree _ _ a t1 t2 ->
  absize_int _ _ (add_abtree _ _ a t1 t2) = S (absize_int _ _ t1 + absize_int _ _ t2).
Proof.
induction t1.
- intros.
  simpl.
  reflexivity.
- intros.
  tauto.
Qed. *)

Lemma S_aux: forall (n m: nat), S (suml n m) = suml (S n) m.
Proof.
tauto.
Qed.

Lemma S_aux2: forall (n m: nat), S (suml n m) = suml n (S m).
Proof.
intros x y.
rewrite (SumS x y).
tauto.
Qed.

Lemma rel_ext_int: forall (A B: Set) (t: ABTree A B), absize_ext _ _ t = S (absize_int _ _ t).
Proof.
induction t; simpl.
- reflexivity.
- rewrite IHt1.
  rewrite IHt2.
  rewrite (S_aux (absize_int A B t1) (absize_int A B t2)).
  rewrite (S_aux2 (S (absize_int A B t1)) (absize_int A B t2)).
  reflexivity.
Qed.

Variable A : Set.

Lemma subarbol_refl: forall (t: Tree_ A), subarbol _ t t.
Proof.
induction t.
- exact (sub_id _ (nullT _)).
- exact (sub_id _ (consT _ a t1 t2)).
Qed.

Lemma null_subarbol: forall (t: Tree_ A), subarbol _ (nullT A) t.
Proof.
induction t.
- exact (sub_id _ (nullT _)).
- exact (sub_propio_izq _ (nullT _) t1 t2 a IHt1).
Qed.

Lemma subarbol_trans: forall (t u v: Tree_ A), subarbol A t u -> subarbol A u v -> subarbol A t v.
Proof.
intros t u v h0 h1.
induction h1.
- assumption.
- apply sub_propio_izq.
  apply IHh1.
  assumption.
- apply sub_propio_der.
  apply IHh1.
  assumption.
Qed.

Lemma comtsize: forall (n:nat) (A:Set) (t: ComTree A n), h _ _ t = pot 2 n.
Proof.
induction t.
- (* rewrite (potO 1). *)
  simpl.
  reflexivity.
- rewrite (potS n).
  simpl.
  rewrite IHt1.
  rewrite IHt2.
  reflexivity.
Qed.

Lemma largo_camino: forall (n: nat) (A: Set) (t: AB A n), llength _ (camino n A t) = n.
Proof.
induction t; simpl.
- reflexivity.
- case_eq (le_bool m n); intro le_value ; simpl.
  * rewrite IHt1.
    rewrite (max_r m n).
    + reflexivity.
    + assumption.
  * rewrite IHt2.
    rewrite (max_l m n).
    + reflexivity.
    + assumption.
Qed.

End Pruebas.