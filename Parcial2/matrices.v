Section Matrices.

Inductive array (A:Set): nat -> Set :=
  | empty_a : array A 0
  | add_a : forall n:nat, A -> array A n -> array A (S n).

Inductive matrix (A:Set): nat -> nat -> Set :=
  | one_col : forall n:nat, array A n -> matrix A 1 n
  | extend_col : forall m n:nat, matrix A m n -> array A n -> matrix A (S m) n.

End Matrices.